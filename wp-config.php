<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'db_aov');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '}$I?jy]PjL4O&-x}xeXcK?=ss)MtM+21l@xG^*Yxh6ljxVcSzJ3Ex~L]=UQL;3Yi');
define('SECURE_AUTH_KEY',  '3KS<iP7jkJX5g^ @` XgZ$!FEtMlsb>>)YxVbQ82-YD%fn_b62~&43o`Q1:$o,=a');
define('LOGGED_IN_KEY',    'SD6=RMI9>+e70mPLF6 /xH>$->^5XhgiIAK~YhT:d)85ogC^a0~rxfUU*^>DrjYh');
define('NONCE_KEY',        ',~}:|qnP75w ~O!T<H5M{[GpbUx2Tf$$[&)BR1x)nEyj[?i[;BPhKZD(FAUKa*Ss');
define('AUTH_SALT',        '-/A29lZQpykZK[jap9f#+kFM(=NoAYOh<pc:a/bWzFQ!UuLiC@PqHjLimK65Xnc{');
define('SECURE_AUTH_SALT', 'sv1!fJ;9x|XiNb(d+o_kSu()Y?oTEZ(|qRu6DS7G4lH7szH[pb F+J`UG*o)gKWH');
define('LOGGED_IN_SALT',   '~j6W$0*zeA)Ge}AoH)HK?G3zW$o,D[d[H>9hsK^8W8[arap7>OG+Z9Z`}4ioodJV');
define('NONCE_SALT',       '@ND#l;Duh=h{yaU63bKs;lHU6+me18#(eY(s{r<c4OxL-3c`qtaoH8}?!8j8`~t>');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
