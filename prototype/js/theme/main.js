$(document).ready(function () {
  $('.show-stats').click(function(){
    $(this).next().slideToggle();
  });

  /* Outbound link tracking */
  $("a.tracked").click(function (e) {
    if (!ga.q) {

      var url = $(this).attr("href");
      ga("send", "event", "outbound", "click", url, {
        "hitCallback":
          function () {
            document.location = url;
          }
      });
      e.preventDefault();
    }
  });

  /* FASTCLICK */
  FastClick.attach(document.body);

  //Tables for Tier Lists
  table = $(".tier-list").stupidtable();

  table.bind('aftertablesort', function (event, data) {

    refreshLazyLoadedImages();

  });

  /* DIQUS */
  var disqus_div = $("#disqus_thread");

  if (disqus_div.length > 0) {
    var ds_loaded = false,
      top = disqus_div.offset().top, // WHERE TO START LOADING
      disqus_data = disqus_div.data(),
      check = function () {
        if (!ds_loaded && $(window).scrollTop() + $(window).height() > top) {
          ds_loaded = true;
          for (var key in disqus_data) {
            if (key.substr(0, 6) == 'disqus') {
              window['disqus_' + key.replace('disqus', '').toLowerCase()] = disqus_data[key];
            }
          }

          var dsq = document.createElement('script');
          dsq.type = 'text/javascript';
          dsq.async = true;
          dsq.src = '//' + window.disqus_shortname + '.disqus.com/embed.js';
          (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        }
      };

    $(window).scroll(check);
    check();
  }

  /* SEARCH BAR ON DESKTOP */
  var midTransition = false;

  function instantSearch(query, resultsEl) {

    if (query.length > 3) {

      $.post(ajaxurl, {'s': query, 'action': 'search'}, function (data) {

        if (data == '') {
          resultsEl.hide();
        } else {
          resultsEl.fadeIn();
        }

        resultsEl.html(data);

      });

      return true;

    } else {
      resultsEl.hide();

      return false;
    }

  }

  $('#site-search-input').keyup(function () {

    var query = $(this).val();
    var resultsEl = $('#site-search-results');

    instantSearch(query, resultsEl);


  });

  $('#site-search-input').blur(function () {

    if (midTransition) return;
    midTransition = true;

    setTimeout(function () {

      $('#site-search').removeClass('show');
      $('#site-bar-menu').fadeIn('fast', function () {
        midTransition = false;
      });

      $('#site-search-results').hide();

    }, 100);


  });

  $('#site-search i').click(function () {

    if (midTransition) return;
    midTransition = true;

    if (!$('#site-search').hasClass('show')) {

      setTimeout(function () {
        $('#site-search-input').val('').focus();
      }, 1);


    } else {
      $('#site-search-results').hide()
    }

    $('#site-search').toggleClass('show');
    $('#site-bar-menu').fadeToggle('fast', function () {
      midTransition = false;
    });

  });

  /* MOBILE SEARCH */
  var mobileSearchEl = $('#mobile-site-search');
  var mobileSearchInput = $('#mobile-site-search input');
  var mobileSearchResults = $('#mobile-site-search-results');
  var mobileSearchClear = $('#mobile-search-clear');

  $('#toggleMobileSearch').click(function (e) {

    e.preventDefault();
    e.stopPropagation();

    if (mobileSearchEl.is(':visible')) {
      mobileSearchEl.hide();
      mobileSearchInput.blur();

    } else {
      mobileSearchEl.show();
      mobileSearchInput.focus();
    }

  });

  mobileSearchInput.keyup(function () {

    var query = $(this).val();
    var resultsEl = mobileSearchResults;

    instantSearch(query, resultsEl);

    if (query.length == 0) {

      mobileSearchClear.hide();

    } else {

      mobileSearchClear.show();

    }

  });

  mobileSearchClear.click(function (e) {

    mobileSearchInput.val('');
    mobileSearchEl.hide();
    mobileSearchResults.hide();

  });


  /* STAT TRACKING */
  var TYPE_VIEW = 'viw';
  var TYPE_RATING = 'rtg';
  var TYPE_POLL = 'pll';
  var TYPE_FAVORITE = 'fav';
  var TYPE_UP_DOWN_VOTE = 'udv';

  function getStats(ids) {

    var data = {
      'stat-request': ids,
      'action': 'get_stat'
    };

    $.post(ajaxurl, data, function (data) {

      data = $.parseJSON(data);

      if (data.message == "success") {

        var values = data.value;

        $.each(values, function (key, value) {

          var el = $('#' + key);
          var type = el.attr('data-track');
          updateStatUI(el, type, value, false);

        });

      }

    });

  }

  function updateStatUI(el, type, value, adjustCounter) {

    if (type == TYPE_FAVORITE) {

      var clickEl = el.find('[data-click-track]');

      if (value == 1) {
        clickEl.attr('data-click-value', 0);
        el.addClass('voted');
      } else {
        clickEl.attr('data-click-value', 1);
        el.removeClass('voted');
      }

      if (adjustCounter) {

        var counterEl = el.find('[data-track-show]');
        var count = counterEl.attr('data-track-show-value');

        if (value == 1) {
          count++;
          counterEl.attr('data-track-show-value', formatStatValue(count));
          counterEl.text(count);
        } else {
          count--;
          counterEl.text(count);
          counterEl.attr('data-track-show-value', formatStatValue(count));
        }
      }

    }

  }

  function formatStatValue(num) {
    return num > 999 ? (num / 1000).toFixed(1) + 'k' : num;
  }

  function track(postID, group, subgroup, value, callback) {

    var data = {
      'post_id': postID,
      'group': group,
      'subgroup': subgroup,
      'value': value,
      'action': 'track_stat'
    };

    $.post(ajaxurl, data, function (data) {

      data = $.parseJSON(data);
      console.log(data);
      if (callback) callback(data.value);

    });

  }

  var trackGroups = [];

  $('[data-track]').each(function () {

    var groupEl = $(this);
    var id = $(this).attr('id');
    var data = id.split('--');
    var group = data[0];
    var postID = data[1];
    var type = $(this).attr('data-track');

    trackGroups.push(id);

    //Find children that want to track when clicked
    $(this).find('[data-click-track]').click(function () {

      var clickEl = $(this);
      var subgroup = clickEl.attr('data-click-track');
      var value = clickEl.attr('data-click-value');

      track(postID, group, subgroup, value, function (newValue) {

        updateStatUI(groupEl, type, newValue, true);

      });
    });
  });

  if (trackGroups.length > 0) getStats(trackGroups);

  //Used for auto tracking stat groups
  //example <span data-auto-track="view" data-post-id="POST-ID" class="hide"></span>
  $('[data-auto-track]').each(function () {

    var group = $(this).attr('data-auto-track');
    var postID = $(this).attr('data-post-id');

    track(postID, group, '', 1);

  });


  /* -- LAZY LOAD IMAGES ---*/
  var bLazy = new Blazy({
    loadInvisible: true,
    breakpoints: [{
      width: 420,
      src: 'data-src-small'
    }]
  });

  function refreshLazyLoadedImages() {
    bLazy.revalidate();
  }

  /* --- LOCAL SEARCH AND FILTER -- */
  $localArchiveFiltering = $('.archive-ui-local');

  if ($localArchiveFiltering && $localArchiveFiltering.length > 0) {

    $filterUI = $($localArchiveFiltering[0]);

    $('.archive-ui-local .toggle-btn').on('click', function (e) {

      $(this).parent().children().removeClass('selected');
      $(this).addClass('selected');

      filterLocalElements();

    });

    $('.archive-ui-local .local-search').on('keyup', function (e) {

      filterLocalElements();

    });


    function filterLocalElements() {

      var filterList = [];

      $('.archive-ui-local [data-filter-click].selected').each(function () {

        var el = $(this);
        var filter = el.attr('data-filter-click');
        var equals = el.attr('data-filter-value');

        filterList.push({
          'filter': filter,
          'equals': equals,
          'compare': 'exact'
        });

      });

      $('.archive-ui-local .local-search').each(function () {

        var el = $(this);
        var filter = el.attr('data-filter-search');
        var equals = el.val();

        if (equals == '') return;

        filterList.push({
          'filter': filter,
          'equals': equals,
          'compare': 'search'
        })

      });

      var filterListCount = filterList.length;

      $('.searchable').each(function () {

        var element = $(this);
        var found = true;

        for (var i = 0; i < filterListCount; i++) {

          var filterData = filterList[i];
          var filterValue = filterData['equals'];
          var elementValue = element.attr(filterData['filter'])

          if (filterValue == ':any:') continue;

          if (filterData['compare'] == 'search') {

            elementValue = elementValue.toLowerCase();
            filterValue = filterValue.toLowerCase();

            if (elementValue.indexOf(filterValue) == -1) {
              found = false;
            }

          } else {

            if (elementValue != filterValue) {
              found = false;
            }

          }

        }

        if (found) {
          element.show();
        } else {
          element.hide();
        }

      });

      refreshLazyLoadedImages();
    }


  }


  /* --- ARCHIVE INTERFACE ELEMENTS --- */
  if (typeof ACTIVE_FILTER !== 'undefined') {

    var filters = ACTIVE_FILTER.split(',');

    for (var i = 0; i < filters.length; i++) {

      var filterString = filters[i];
      var filterParts = filterString.split(':');

      $('[data-query="' + filterParts[0] + ':' + filterParts[1] + '"]').val(filterParts[2]);

    }

  }


  $('[data-toggle]').click(function (e) {

    e.preventDefault();

    var toggleTarget = $(this).attr('data-toggle');
    $(toggleTarget).slideToggle();

    var text = $(this).text();
    var toggleText = $(this).attr('data-toggle-text');

    $(this).text(toggleText);
    $(this).attr('data-toggle-text', text);


    return false;
  });

  $('#archive-search').on('keypress', function (e) {

    if (e.which === 13) {

      runArchiveQuery();

    }

  });

  $('.archive-ui [data-activate]').click(function (e) {

    var selector = $(this).attr('data-activate');
    var el = $(selector);

    runArchiveQuery();

  });

  $('.archive-ui select').change(function () {

    runArchiveQuery();

  });

  function runArchiveQuery() {

    var data = [];

    $('[data-query]').each(function () {
      var key = $(this).attr('data-query');
      var val = $(this).val();

      if (val != '') {

        data.push([key + ':' + val]);

      }
    });

    window.location.href = window.location.href.replace(/[\?#].*|$/, "?filter=" + data.join(','));

  }


  /* --- MOBILE MENU --- */
  $('#toggleMobileMenu').click(function (e) {

    $('body').toggleClass('mobile-menu-active');

  });


  /* --- SMOOTH SCROLL LINKS AND BACK TO TOP --- */
  $('a[href*="#"]:not([href="#"])').click(function () {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 500);
        return false;
      }
    }
  });

  function handleScroll() {
    if ($(window).scrollTop()) {
      $('.back-to-top:hidden').stop(true, true).fadeIn();
    } else {
      $('.back-to-top').stop(true, true).fadeOut();
    }
  }

  handleScroll();

  $(window).scroll(function () {
    handleScroll();
  });


  /* --- TOOLTIPS --- */
  /* POST RELATED TOOLTIPS */
  $('.tooltip').tooltipster({
    theme: 'tooltipster-borderless',
    trigger: 'hover',
    interactive: false
  });

  $('a[data-post-tip]').tooltipster({
    animation: 'fade',
    updateAnimation: 'fade',
    contentAsHTML: true,
    interactive: true,
    minWidth: 250,
    theme: 'tooltipster-borderless',
    distance: 20,
    content: 'Loading...',

    functionBefore: function (instance, helper) {

      var $origin = $(helper.origin);

      // we set a variable so the data is only loaded once via Ajax, not every time the tooltip opens
      if ($origin.data('loaded') !== true) {

        $.ajax({
          type: "POST",
          url: ajaxurl,
          data: {
            action: "loadPostContent",
            post_id: $origin.data('post-tip')
          },
          success: function (data) {

            var el = $(data);

            window.setTimeout(function () {

              instance.content(data);
              $origin.data('loaded', true);

            }, 300);


          }
        });
      }
    }
  });


  /* SELECT INPUT FOR SHORTLINKS */
  $('.select-input').click(function () {

    $(this).select();

  });

});


/* Ajax Login and Register */

/* Login and Register */
function open_login_dialog(href) {

  var modal = jQuery('#user-modal');
  show_dialog_section(href);
  modal.fadeIn();

}

function close_login_dialog() {

  jQuery('#user-modal').fadeOut();
}

function show_dialog_section(sectionSelector) {

  jQuery('#user-modal section').hide();
  jQuery('#user-modal section' + sectionSelector).show();

}

function button_loading(button) {
  button.text(button.attr('data-loading-text'));
}

function button_normal(button) {
  button.text(button.attr('data-text'));
}

function url_param(name) {
  var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
  return results[1] || 0;
}

jQuery(function ($) {

  var url = window.location.href;

  if (url.indexOf('?login=') != -1) {

    open_login_dialog('#login');

  }

  if (url.indexOf('?reset=') != -1) {

    open_login_dialog('#password-reset');

  }

  if (url.indexOf('?activate=') != -1) {

    open_login_dialog('#activating');

    var activate = url_param('activate');


    var data = {'action': 'register_activate', 'activate': activate};

    $.post(ajaxurl, data, function (data) {

      var obj = $.parseJSON(data);

      if (obj.success == true) {
        window.location = window.location.pathname;
      } else {
        show_dialog_section('#new-activation-email')
      }

    });

  }

  // Open login/register modal
  $('[href="#login"], [href="#register"], [href="#lost-password"]').click(function (e) {

    e.preventDefault();

    open_login_dialog($(this).attr('href'));

  });

  // Switch forms login/register
  $('a[href="#reset-password"]').click(function (e) {
    e.preventDefault();
    show_dialog_section('#reset-password');
  });

  $('#user-modal .cancel').click(function (e) {
    close_login_dialog();
  });

  // Post login form
  $('#login-form').on('submit', function (e) {

    var form = $(this);
    e.preventDefault();

    var button = form.find('.submit');
    button_loading(button);

    $.post(ajaxurl, form.serialize(), function (data) {

      var obj = $.parseJSON(data);

      if (obj.success == true) {
        show_dialog_section('#loading');
        window.location.reload(true);
      }

      form.parent().find('.errors').html(obj.message);
      button_normal(button);
    });

  });


  // Post register form
  $('#registration-form').on('submit', function (e) {

    var form = $(this);
    e.preventDefault();

    var button = form.find('.submit');
    button_loading(button);

    $.post(ajaxurl, form.serialize(), function (data) {

      var obj = $.parseJSON(data);

      if (obj.success == true) {
        show_dialog_section('#registration-complete');
      }

      form.parent().find('.errors').html(obj.message);
      button_normal(button);

    });

  });


  $('#reset-form').on('submit', function (e) {

    var form = $(this);
    e.preventDefault();

    var button = form.find('.submit');
    button_loading(button);

    var reset = url_param('reset');
    var data = form.serialize() + "&reset=" + reset;

    $.post(ajaxurl, data, function (data) {

      var obj = $.parseJSON(data);

      if (obj.success == true) {
        show_dialog_section('#loading');
        window.location = window.location.pathname;
      }

      form.parent().find('.errors').html(obj.message);
      button_normal(button);
    });

  });

  $('#forgot-form').on('submit', function (e) {

    var form = $(this);
    e.preventDefault();

    var button = form.find('.submit');
    button_loading(button);

    var data = form.serialize();

    $.post(ajaxurl, data, function (data) {

      var obj = $.parseJSON(data);

      if (obj.success == true) {
        show_dialog_section('#lost-sent');
      }

      form.parent().find('.errors').html(obj.message);
      button_normal(button);
    });

  });

  clipboard = new Clipboard('.copy');

  clipboard.on('success', function (e) {

    $(e.trigger).html('COPIED');

  });

});

/* Copy to Clipboard */
/*!
 * clipboard.js v1.6.0
 * https://zenorocha.github.io/clipboard.js
 *
 * Licensed MIT © Zeno Rocha
 */
!function (e) {
  if ("object" == typeof exports && "undefined" != typeof module) module.exports = e(); else if ("function" == typeof define && define.amd) define([], e); else {
    var t;
    t = "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : this, t.Clipboard = e()
  }
}(function () {
  var e, t, n;
  return function e(t, n, o) {
    function i(a, c) {
      if (!n[a]) {
        if (!t[a]) {
          var l = "function" == typeof require && require;
          if (!c && l) return l(a, !0);
          if (r) return r(a, !0);
          var u = new Error("Cannot find module '" + a + "'");
          throw u.code = "MODULE_NOT_FOUND", u
        }
        var s = n[a] = {exports: {}};
        t[a][0].call(s.exports, function (e) {
          var n = t[a][1][e];
          return i(n ? n : e)
        }, s, s.exports, e, t, n, o)
      }
      return n[a].exports
    }

    for (var r = "function" == typeof require && require, a = 0; a < o.length; a++) i(o[a]);
    return i
  }({
    1: [function (e, t, n) {
      function o(e, t) {
        for (; e && e.nodeType !== i;) {
          if (e.matches(t)) return e;
          e = e.parentNode
        }
      }

      var i = 9;
      if (Element && !Element.prototype.matches) {
        var r = Element.prototype;
        r.matches = r.matchesSelector || r.mozMatchesSelector || r.msMatchesSelector || r.oMatchesSelector || r.webkitMatchesSelector
      }
      t.exports = o
    }, {}], 2: [function (e, t, n) {
      function o(e, t, n, o, r) {
        var a = i.apply(this, arguments);
        return e.addEventListener(n, a, r), {
          destroy: function () {
            e.removeEventListener(n, a, r)
          }
        }
      }

      function i(e, t, n, o) {
        return function (n) {
          n.delegateTarget = r(n.target, t), n.delegateTarget && o.call(e, n)
        }
      }

      var r = e("./closest");
      t.exports = o
    }, {"./closest": 1}], 3: [function (e, t, n) {
      n.node = function (e) {
        return void 0 !== e && e instanceof HTMLElement && 1 === e.nodeType
      }, n.nodeList = function (e) {
        var t = Object.prototype.toString.call(e);
        return void 0 !== e && ("[object NodeList]" === t || "[object HTMLCollection]" === t) && "length" in e && (0 === e.length || n.node(e[0]))
      }, n.string = function (e) {
        return "string" == typeof e || e instanceof String
      }, n.fn = function (e) {
        var t = Object.prototype.toString.call(e);
        return "[object Function]" === t
      }
    }, {}], 4: [function (e, t, n) {
      function o(e, t, n) {
        if (!e && !t && !n) throw new Error("Missing required arguments");
        if (!c.string(t)) throw new TypeError("Second argument must be a String");
        if (!c.fn(n)) throw new TypeError("Third argument must be a Function");
        if (c.node(e)) return i(e, t, n);
        if (c.nodeList(e)) return r(e, t, n);
        if (c.string(e)) return a(e, t, n);
        throw new TypeError("First argument must be a String, HTMLElement, HTMLCollection, or NodeList")
      }

      function i(e, t, n) {
        return e.addEventListener(t, n), {
          destroy: function () {
            e.removeEventListener(t, n)
          }
        }
      }

      function r(e, t, n) {
        return Array.prototype.forEach.call(e, function (e) {
          e.addEventListener(t, n)
        }), {
          destroy: function () {
            Array.prototype.forEach.call(e, function (e) {
              e.removeEventListener(t, n)
            })
          }
        }
      }

      function a(e, t, n) {
        return l(document.body, e, t, n)
      }

      var c = e("./is"), l = e("delegate");
      t.exports = o
    }, {"./is": 3, delegate: 2}], 5: [function (e, t, n) {
      function o(e) {
        var t;
        if ("SELECT" === e.nodeName) e.focus(), t = e.value; else if ("INPUT" === e.nodeName || "TEXTAREA" === e.nodeName) {
          var n = e.hasAttribute("readonly");
          n || e.setAttribute("readonly", ""), e.select(), e.setSelectionRange(0, e.value.length), n || e.removeAttribute("readonly"), t = e.value
        } else {
          e.hasAttribute("contenteditable") && e.focus();
          var o = window.getSelection(), i = document.createRange();
          i.selectNodeContents(e), o.removeAllRanges(), o.addRange(i), t = o.toString()
        }
        return t
      }

      t.exports = o
    }, {}], 6: [function (e, t, n) {
      function o() {
      }

      o.prototype = {
        on: function (e, t, n) {
          var o = this.e || (this.e = {});
          return (o[e] || (o[e] = [])).push({fn: t, ctx: n}), this
        }, once: function (e, t, n) {
          function o() {
            i.off(e, o), t.apply(n, arguments)
          }

          var i = this;
          return o._ = t, this.on(e, o, n)
        }, emit: function (e) {
          var t = [].slice.call(arguments, 1), n = ((this.e || (this.e = {}))[e] || []).slice(), o = 0, i = n.length;
          for (o; o < i; o++) n[o].fn.apply(n[o].ctx, t);
          return this
        }, off: function (e, t) {
          var n = this.e || (this.e = {}), o = n[e], i = [];
          if (o && t) for (var r = 0, a = o.length; r < a; r++) o[r].fn !== t && o[r].fn._ !== t && i.push(o[r]);
          return i.length ? n[e] = i : delete n[e], this
        }
      }, t.exports = o
    }, {}], 7: [function (t, n, o) {
      !function (i, r) {
        if ("function" == typeof e && e.amd) e(["module", "select"], r); else if ("undefined" != typeof o) r(n, t("select")); else {
          var a = {exports: {}};
          r(a, i.select), i.clipboardAction = a.exports
        }
      }(this, function (e, t) {
        "use strict";

        function n(e) {
          return e && e.__esModule ? e : {default: e}
        }

        function o(e, t) {
          if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        var i = n(t), r = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
          return typeof e
        } : function (e) {
          return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
        }, a = function () {
          function e(e, t) {
            for (var n = 0; n < t.length; n++) {
              var o = t[n];
              o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(e, o.key, o)
            }
          }

          return function (t, n, o) {
            return n && e(t.prototype, n), o && e(t, o), t
          }
        }(), c = function () {
          function e(t) {
            o(this, e), this.resolveOptions(t), this.initSelection()
          }

          return a(e, [{
            key: "resolveOptions", value: function e() {
              var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
              this.action = t.action, this.emitter = t.emitter, this.target = t.target, this.text = t.text, this.trigger = t.trigger, this.selectedText = ""
            }
          }, {
            key: "initSelection", value: function e() {
              this.text ? this.selectFake() : this.target && this.selectTarget()
            }
          }, {
            key: "selectFake", value: function e() {
              var t = this, n = "rtl" == document.documentElement.getAttribute("dir");
              this.removeFake(), this.fakeHandlerCallback = function () {
                return t.removeFake()
              }, this.fakeHandler = document.body.addEventListener("click", this.fakeHandlerCallback) || !0, this.fakeElem = document.createElement("textarea"), this.fakeElem.style.fontSize = "12pt", this.fakeElem.style.border = "0", this.fakeElem.style.padding = "0", this.fakeElem.style.margin = "0", this.fakeElem.style.position = "absolute", this.fakeElem.style[n ? "right" : "left"] = "-9999px";
              var o = window.pageYOffset || document.documentElement.scrollTop;
              this.fakeElem.style.top = o + "px", this.fakeElem.setAttribute("readonly", ""), this.fakeElem.value = this.text, document.body.appendChild(this.fakeElem), this.selectedText = (0, i.default)(this.fakeElem), this.copyText()
            }
          }, {
            key: "removeFake", value: function e() {
              this.fakeHandler && (document.body.removeEventListener("click", this.fakeHandlerCallback), this.fakeHandler = null, this.fakeHandlerCallback = null), this.fakeElem && (document.body.removeChild(this.fakeElem), this.fakeElem = null)
            }
          }, {
            key: "selectTarget", value: function e() {
              this.selectedText = (0, i.default)(this.target), this.copyText()
            }
          }, {
            key: "copyText", value: function e() {
              var t = void 0;
              try {
                t = document.execCommand(this.action)
              } catch (e) {
                t = !1
              }
              this.handleResult(t)
            }
          }, {
            key: "handleResult", value: function e(t) {
              this.emitter.emit(t ? "success" : "error", {
                action: this.action,
                text: this.selectedText,
                trigger: this.trigger,
                clearSelection: this.clearSelection.bind(this)
              })
            }
          }, {
            key: "clearSelection", value: function e() {
              this.target && this.target.blur(), window.getSelection().removeAllRanges()
            }
          }, {
            key: "destroy", value: function e() {
              this.removeFake()
            }
          }, {
            key: "action", set: function e() {
              var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "copy";
              if (this._action = t, "copy" !== this._action && "cut" !== this._action) throw new Error('Invalid "action" value, use either "copy" or "cut"')
            }, get: function e() {
              return this._action
            }
          }, {
            key: "target", set: function e(t) {
              if (void 0 !== t) {
                if (!t || "object" !== ("undefined" == typeof t ? "undefined" : r(t)) || 1 !== t.nodeType) throw new Error('Invalid "target" value, use a valid Element');
                if ("copy" === this.action && t.hasAttribute("disabled")) throw new Error('Invalid "target" attribute. Please use "readonly" instead of "disabled" attribute');
                if ("cut" === this.action && (t.hasAttribute("readonly") || t.hasAttribute("disabled"))) throw new Error('Invalid "target" attribute. You can\'t cut text from elements with "readonly" or "disabled" attributes');
                this._target = t
              }
            }, get: function e() {
              return this._target
            }
          }]), e
        }();
        e.exports = c
      })
    }, {select: 5}], 8: [function (t, n, o) {
      !function (i, r) {
        if ("function" == typeof e && e.amd) e(["module", "./clipboard-action", "tiny-emitter", "good-listener"], r); else if ("undefined" != typeof o) r(n, t("./clipboard-action"), t("tiny-emitter"), t("good-listener")); else {
          var a = {exports: {}};
          r(a, i.clipboardAction, i.tinyEmitter, i.goodListener), i.clipboard = a.exports
        }
      }(this, function (e, t, n, o) {
        "use strict";

        function i(e) {
          return e && e.__esModule ? e : {default: e}
        }

        function r(e, t) {
          if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function a(e, t) {
          if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
          return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function c(e, t) {
          if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
          e.prototype = Object.create(t && t.prototype, {
            constructor: {
              value: e,
              enumerable: !1,
              writable: !0,
              configurable: !0
            }
          }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }

        function l(e, t) {
          var n = "data-clipboard-" + e;
          if (t.hasAttribute(n)) return t.getAttribute(n)
        }

        var u = i(t), s = i(n), f = i(o), d = function () {
          function e(e, t) {
            for (var n = 0; n < t.length; n++) {
              var o = t[n];
              o.enumerable = o.enumerable || !1, o.configurable = !0, "value" in o && (o.writable = !0), Object.defineProperty(e, o.key, o)
            }
          }

          return function (t, n, o) {
            return n && e(t.prototype, n), o && e(t, o), t
          }
        }(), h = function (e) {
          function t(e, n) {
            r(this, t);
            var o = a(this, (t.__proto__ || Object.getPrototypeOf(t)).call(this));
            return o.resolveOptions(n), o.listenClick(e), o
          }

          return c(t, e), d(t, [{
            key: "resolveOptions", value: function e() {
              var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
              this.action = "function" == typeof t.action ? t.action : this.defaultAction, this.target = "function" == typeof t.target ? t.target : this.defaultTarget, this.text = "function" == typeof t.text ? t.text : this.defaultText
            }
          }, {
            key: "listenClick", value: function e(t) {
              var n = this;
              this.listener = (0, f.default)(t, "click", function (e) {
                return n.onClick(e)
              })
            }
          }, {
            key: "onClick", value: function e(t) {
              var n = t.delegateTarget || t.currentTarget;
              this.clipboardAction && (this.clipboardAction = null), this.clipboardAction = new u.default({
                action: this.action(n),
                target: this.target(n),
                text: this.text(n),
                trigger: n,
                emitter: this
              })
            }
          }, {
            key: "defaultAction", value: function e(t) {
              return l("action", t)
            }
          }, {
            key: "defaultTarget", value: function e(t) {
              var n = l("target", t);
              if (n) return document.querySelector(n)
            }
          }, {
            key: "defaultText", value: function e(t) {
              return l("text", t)
            }
          }, {
            key: "destroy", value: function e() {
              this.listener.destroy(), this.clipboardAction && (this.clipboardAction.destroy(), this.clipboardAction = null)
            }
          }], [{
            key: "isSupported", value: function e() {
              var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : ["copy", "cut"],
                n = "string" == typeof t ? [t] : t, o = !!document.queryCommandSupported;
              return n.forEach(function (e) {
                o = o && !!document.queryCommandSupported(e)
              }), o
            }
          }]), t
        }(s.default);
        e.exports = h
      })
    }, {"./clipboard-action": 7, "good-listener": 4, "tiny-emitter": 6}]
  }, {}, [8])(8)
});

/* Lazy Load Images */
/*!
 hey, [be]Lazy.js - v1.8.2 - 2016.10.25
 A fast, small and dependency free lazy load script (https://github.com/dinbror/blazy)
 (c) Bjoern Klinggaard - @bklinggaard - http://dinbror.dk/blazy
 */
(function (q, m) {
  "function" === typeof define && define.amd ? define(m) : "object" === typeof exports ? module.exports = m() : q.Blazy = m()
})(this, function () {
  function q(b) {
    var c = b._util;
    c.elements = E(b.options);
    c.count = c.elements.length;
    c.destroyed && (c.destroyed = !1, b.options.container && l(b.options.container, function (a) {
      n(a, "scroll", c.validateT)
    }), n(window, "resize", c.saveViewportOffsetT), n(window, "resize", c.validateT), n(window, "scroll", c.validateT));
    m(b)
  }

  function m(b) {
    for (var c = b._util, a = 0; a < c.count; a++) {
      var d = c.elements[a], e;
      a:{
        var g = d;
        e = b.options;
        var p = g.getBoundingClientRect();
        if (e.container && y && (g = g.closest(e.containerClass))) {
          g = g.getBoundingClientRect();
          e = r(g, f) ? r(p, {
            top: g.top - e.offset,
            right: g.right + e.offset,
            bottom: g.bottom + e.offset,
            left: g.left - e.offset
          }) : !1;
          break a
        }
        e = r(p, f)
      }
      if (e || t(d, b.options.successClass)) b.load(d), c.elements.splice(a, 1), c.count--, a--
    }
    0 === c.count && b.destroy()
  }

  function r(b, c) {
    return b.right >= c.left && b.bottom >= c.top && b.left <= c.right && b.top <= c.bottom
  }

  function z(b, c, a) {
    if (!t(b, a.successClass) && (c || a.loadInvisible || 0 < b.offsetWidth && 0 < b.offsetHeight)) if (c = b.getAttribute(u) || b.getAttribute(a.src)) {
      c = c.split(a.separator);
      var d = c[A && 1 < c.length ? 1 : 0], e = b.getAttribute(a.srcset), g = "img" === b.nodeName.toLowerCase(),
        p = (c = b.parentNode) && "picture" === c.nodeName.toLowerCase();
      if (g || void 0 === b.src) {
        var h = new Image, w = function () {
          a.error && a.error(b, "invalid");
          v(b, a.errorClass);
          k(h, "error", w);
          k(h, "load", f)
        }, f = function () {
          g ? p || B(b, d, e) : b.style.backgroundImage = 'url("' + d + '")';
          x(b, a);
          k(h, "load", f);
          k(h, "error", w)
        };
        p && (h = b, l(c.getElementsByTagName("source"), function (b) {
          var c = a.srcset, e = b.getAttribute(c);
          e && (b.setAttribute("srcset", e), b.removeAttribute(c))
        }));
        n(h, "error", w);
        n(h, "load", f);
        B(h, d, e)
      } else b.src = d, x(b, a)
    } else "video" === b.nodeName.toLowerCase() ? (l(b.getElementsByTagName("source"), function (b) {
      var c = a.src, e = b.getAttribute(c);
      e && (b.setAttribute("src", e), b.removeAttribute(c))
    }), b.load(), x(b, a)) : (a.error && a.error(b, "missing"), v(b, a.errorClass))
  }

  function x(b, c) {
    v(b, c.successClass);
    c.success && c.success(b);
    b.removeAttribute(c.src);
    b.removeAttribute(c.srcset);
    l(c.breakpoints, function (a) {
      b.removeAttribute(a.src)
    })
  }

  function B(b, c, a) {
    a && b.setAttribute("srcset", a);
    b.src = c
  }

  function t(b, c) {
    return -1 !== (" " + b.className + " ").indexOf(" " + c + " ")
  }

  function v(b, c) {
    t(b, c) || (b.className += " " + c)
  }

  function E(b) {
    var c = [];
    b = b.root.querySelectorAll(b.selector);
    for (var a = b.length; a--; c.unshift(b[a])) ;
    return c
  }

  function C(b) {
    f.bottom = (window.innerHeight || document.documentElement.clientHeight) + b;
    f.right = (window.innerWidth || document.documentElement.clientWidth) + b
  }

  function n(b, c, a) {
    b.attachEvent ? b.attachEvent && b.attachEvent("on" + c, a) : b.addEventListener(c, a, {capture: !1, passive: !0})
  }

  function k(b, c, a) {
    b.detachEvent ? b.detachEvent && b.detachEvent("on" + c, a) : b.removeEventListener(c, a, {
      capture: !1,
      passive: !0
    })
  }

  function l(b, c) {
    if (b && c) for (var a = b.length, d = 0; d < a && !1 !== c(b[d], d); d++) ;
  }

  function D(b, c, a) {
    var d = 0;
    return function () {
      var e = +new Date;
      e - d < c || (d = e, b.apply(a, arguments))
    }
  }

  var u, f, A, y;
  return function (b) {
    if (!document.querySelectorAll) {
      var c = document.createStyleSheet();
      document.querySelectorAll = function (a, b, d, h, f) {
        f = document.all;
        b = [];
        a = a.replace(/\[for\b/gi, "[htmlFor").split(",");
        for (d = a.length; d--;) {
          c.addRule(a[d], "k:v");
          for (h = f.length; h--;) f[h].currentStyle.k && b.push(f[h]);
          c.removeRule(0)
        }
        return b
      }
    }
    var a = this, d = a._util = {};
    d.elements = [];
    d.destroyed = !0;
    a.options = b || {};
    a.options.error = a.options.error || !1;
    a.options.offset = a.options.offset || 100;
    a.options.root = a.options.root || document;
    a.options.success = a.options.success || !1;
    a.options.selector = a.options.selector || ".b-lazy";
    a.options.separator = a.options.separator || "|";
    a.options.containerClass = a.options.container;
    a.options.container = a.options.containerClass ? document.querySelectorAll(a.options.containerClass) : !1;
    a.options.errorClass = a.options.errorClass || "b-error";
    a.options.breakpoints = a.options.breakpoints || !1;
    a.options.loadInvisible = a.options.loadInvisible || !1;
    a.options.successClass = a.options.successClass || "b-loaded";
    a.options.validateDelay = a.options.validateDelay || 25;
    a.options.saveViewportOffsetDelay = a.options.saveViewportOffsetDelay || 50;
    a.options.srcset = a.options.srcset || "data-srcset";
    a.options.src = u = a.options.src || "data-src";
    y = Element.prototype.closest;
    A = 1 < window.devicePixelRatio;
    f = {};
    f.top = 0 - a.options.offset;
    f.left = 0 - a.options.offset;
    a.revalidate = function () {
      q(a)
    };
    a.load = function (a, b) {
      var c = this.options;
      void 0 === a.length ? z(a, b, c) : l(a, function (a) {
        z(a, b, c)
      })
    };
    a.destroy = function () {
      var a = this._util;
      this.options.container && l(this.options.container, function (b) {
        k(b, "scroll", a.validateT)
      });
      k(window, "scroll", a.validateT);
      k(window, "resize", a.validateT);
      k(window, "resize", a.saveViewportOffsetT);
      a.count = 0;
      a.elements.length = 0;
      a.destroyed = !0
    };
    d.validateT = D(function () {
      m(a)
    }, a.options.validateDelay, a);
    d.saveViewportOffsetT = D(function () {
      C(a.options.offset)
    }, a.options.saveViewportOffsetDelay, a);
    C(a.options.offset);
    l(a.options.breakpoints, function (a) {
      if (a.width >= window.screen.width) return u = a.src, !1
    });
    setTimeout(function () {
      q(a)
    })
  }
});


/* Tooltips */
/*! tooltipster v4.1.6 */
!function (a, b) {
  "function" == typeof define && define.amd ? define(["jquery"], function (a) {
    return b(a)
  }) : "object" == typeof exports ? module.exports = b(require("jquery")) : b(jQuery)
}(this, function (a) {
  function b(a) {
    this.$container, this.constraints = null, this.__$tooltip, this.__init(a)
  }

  function c(b, c) {
    var d = !0;
    return a.each(b, function (a, e) {
      return void 0 === c[a] || b[a] !== c[a] ? (d = !1, !1) : void 0
    }), d
  }

  function d(b) {
    var c = b.attr("id"), d = c ? h.window.document.getElementById(c) : null;
    return d ? d === b[0] : a.contains(h.window.document.body, b[0])
  }

  function e() {
    if (!g) return !1;
    var a = g.document.body || g.document.documentElement, b = a.style, c = "transition",
      d = ["Moz", "Webkit", "Khtml", "O", "ms"];
    if ("string" == typeof b[c]) return !0;
    c = c.charAt(0).toUpperCase() + c.substr(1);
    for (var e = 0; e < d.length; e++) if ("string" == typeof b[d[e] + c]) return !0;
    return !1
  }

  var f = {
    animation: "fade",
    animationDuration: 350,
    content: null,
    contentAsHTML: !1,
    contentCloning: !1,
    debug: !0,
    delay: 300,
    delayTouch: [300, 500],
    functionInit: null,
    functionBefore: null,
    functionReady: null,
    functionAfter: null,
    functionFormat: null,
    IEmin: 6,
    interactive: !1,
    multiple: !1,
    parent: "body",
    plugins: ["sideTip"],
    repositionOnScroll: !1,
    restoration: "none",
    selfDestruction: !0,
    theme: [],
    timer: 0,
    trackerInterval: 500,
    trackOrigin: !1,
    trackTooltip: !1,
    trigger: "hover",
    triggerClose: {click: !1, mouseleave: !1, originClick: !1, scroll: !1, tap: !1, touchleave: !1},
    triggerOpen: {click: !1, mouseenter: !1, tap: !1, touchstart: !1},
    updateAnimation: "rotate",
    zIndex: 9999999
  }, g = "undefined" != typeof window ? window : null, h = {
    hasTouchCapability: !(!g || !("ontouchstart" in g || g.DocumentTouch && g.document instanceof g.DocumentTouch || g.navigator.maxTouchPoints)),
    hasTransitions: e(),
    IE: !1,
    semVer: "4.1.6",
    window: g
  }, i = function () {
    this.__$emitterPrivate = a({}), this.__$emitterPublic = a({}), this.__instancesLatestArr = [], this.__plugins = {}, this._env = h
  };
  i.prototype = {
    __bridge: function (b, c, d) {
      if (!c[d]) {
        var e = function () {
        };
        e.prototype = b;
        var g = new e;
        g.__init && g.__init(c), a.each(b, function (a, b) {
          0 != a.indexOf("__") && (c[a] ? f.debug && console.log("The " + a + " method of the " + d + " plugin conflicts with another plugin or native methods") : (c[a] = function () {
            return g[a].apply(g, Array.prototype.slice.apply(arguments))
          }, c[a].bridged = g))
        }), c[d] = g
      }
      return this
    }, __setWindow: function (a) {
      return h.window = a, this
    }, _getRuler: function (a) {
      return new b(a)
    }, _off: function () {
      return this.__$emitterPrivate.off.apply(this.__$emitterPrivate, Array.prototype.slice.apply(arguments)), this
    }, _on: function () {
      return this.__$emitterPrivate.on.apply(this.__$emitterPrivate, Array.prototype.slice.apply(arguments)), this
    }, _one: function () {
      return this.__$emitterPrivate.one.apply(this.__$emitterPrivate, Array.prototype.slice.apply(arguments)), this
    }, _plugin: function (b) {
      var c = this;
      if ("string" == typeof b) {
        var d = b, e = null;
        return d.indexOf(".") > 0 ? e = c.__plugins[d] : a.each(c.__plugins, function (a, b) {
          return b.name.substring(b.name.length - d.length - 1) == "." + d ? (e = b, !1) : void 0
        }), e
      }
      if (b.name.indexOf(".") < 0) throw new Error("Plugins must be namespaced");
      return c.__plugins[b.name] = b, b.core && c.__bridge(b.core, c, b.name), this
    }, _trigger: function () {
      var a = Array.prototype.slice.apply(arguments);
      return "string" == typeof a[0] && (a[0] = {type: a[0]}), this.__$emitterPrivate.trigger.apply(this.__$emitterPrivate, a), this.__$emitterPublic.trigger.apply(this.__$emitterPublic, a), this
    }, instances: function (b) {
      var c = [], d = b || ".tooltipstered";
      return a(d).each(function () {
        var b = a(this), d = b.data("tooltipster-ns");
        d && a.each(d, function (a, d) {
          c.push(b.data(d))
        })
      }), c
    }, instancesLatest: function () {
      return this.__instancesLatestArr
    }, off: function () {
      return this.__$emitterPublic.off.apply(this.__$emitterPublic, Array.prototype.slice.apply(arguments)), this
    }, on: function () {
      return this.__$emitterPublic.on.apply(this.__$emitterPublic, Array.prototype.slice.apply(arguments)), this
    }, one: function () {
      return this.__$emitterPublic.one.apply(this.__$emitterPublic, Array.prototype.slice.apply(arguments)), this
    }, origins: function (b) {
      var c = b ? b + " " : "";
      return a(c + ".tooltipstered").toArray()
    }, setDefaults: function (b) {
      return a.extend(f, b), this
    }, triggerHandler: function () {
      return this.__$emitterPublic.triggerHandler.apply(this.__$emitterPublic, Array.prototype.slice.apply(arguments)), this
    }
  }, a.tooltipster = new i, a.Tooltipster = function (b, c) {
    this.__callbacks = {
      close: [],
      open: []
    }, this.__closingTime, this.__Content, this.__contentBcr, this.__destroyed = !1, this.__destroying = !1, this.__$emitterPrivate = a({}), this.__$emitterPublic = a({}), this.__enabled = !0, this.__garbageCollector, this.__Geometry, this.__lastPosition, this.__namespace = "tooltipster-" + Math.round(1e6 * Math.random()), this.__options, this.__$originParents, this.__pointerIsOverOrigin = !1, this.__previousThemes = [], this.__state = "closed", this.__timeouts = {
      close: [],
      open: null
    }, this.__touchEvents = [], this.__tracker = null, this._$origin, this._$tooltip, this.__init(b, c)
  }, a.Tooltipster.prototype = {
    __init: function (b, c) {
      var d = this;
      if (d._$origin = a(b), d.__options = a.extend(!0, {}, f, c), d.__optionsFormat(), !h.IE || h.IE >= d.__options.IEmin) {
        var e = null;
        if (void 0 === d._$origin.data("tooltipster-initialTitle") && (e = d._$origin.attr("title"), void 0 === e && (e = null), d._$origin.data("tooltipster-initialTitle", e)), null !== d.__options.content) d.__contentSet(d.__options.content); else {
          var g, i = d._$origin.attr("data-tooltip-content");
          i && (g = a(i)), g && g[0] ? d.__contentSet(g.first()) : d.__contentSet(e)
        }
        d._$origin.removeAttr("title").addClass("tooltipstered"), d.__prepareOrigin(), d.__prepareGC(), a.each(d.__options.plugins, function (a, b) {
          d._plug(b)
        }), h.hasTouchCapability && a("body").on("touchmove." + d.__namespace + "-triggerOpen", function (a) {
          d._touchRecordEvent(a)
        }), d._on("created", function () {
          d.__prepareTooltip()
        })._on("repositioned", function (a) {
          d.__lastPosition = a.position
        })
      } else d.__options.disabled = !0
    }, __contentInsert: function () {
      var a = this, b = a._$tooltip.find(".tooltipster-content"), c = a.__Content, d = function (a) {
        c = a
      };
      return a._trigger({
        type: "format",
        content: a.__Content,
        format: d
      }), a.__options.functionFormat && (c = a.__options.functionFormat.call(a, a, {origin: a._$origin[0]}, a.__Content)), "string" != typeof c || a.__options.contentAsHTML ? b.empty().append(c) : b.text(c), a
    }, __contentSet: function (b) {
      return b instanceof a && this.__options.contentCloning && (b = b.clone(!0)), this.__Content = b, this._trigger({
        type: "updated",
        content: b
      }), this
    }, __destroyError: function () {
      throw new Error("This tooltip has been destroyed and cannot execute your method call.")
    }, __geometry: function () {
      var b = this, c = b._$origin, d = b._$origin.is("area");
      if (d) {
        var e = b._$origin.parent().attr("name");
        c = a('img[usemap="#' + e + '"]')
      }
      var f = c[0].getBoundingClientRect(), g = a(h.window.document), i = a(h.window), j = c, k = {
        available: {document: null, window: null},
        document: {size: {height: g.height(), width: g.width()}},
        window: {
          scroll: {
            left: h.window.scrollX || h.window.document.documentElement.scrollLeft,
            top: h.window.scrollY || h.window.document.documentElement.scrollTop
          }, size: {height: i.height(), width: i.width()}
        },
        origin: {
          fixedLineage: !1,
          offset: {},
          size: {height: f.bottom - f.top, width: f.right - f.left},
          usemapImage: d ? c[0] : null,
          windowOffset: {bottom: f.bottom, left: f.left, right: f.right, top: f.top}
        }
      };
      if (d) {
        var l = b._$origin.attr("shape"), m = b._$origin.attr("coords");
        if (m && (m = m.split(","), a.map(m, function (a, b) {
            m[b] = parseInt(a)
          })), "default" != l) switch (l) {
          case"circle":
            var n = m[0], o = m[1], p = m[2], q = o - p, r = n - p;
            k.origin.size.height = 2 * p, k.origin.size.width = k.origin.size.height, k.origin.windowOffset.left += r, k.origin.windowOffset.top += q;
            break;
          case"rect":
            var s = m[0], t = m[1], u = m[2], v = m[3];
            k.origin.size.height = v - t, k.origin.size.width = u - s, k.origin.windowOffset.left += s, k.origin.windowOffset.top += t;
            break;
          case"poly":
            for (var w = 0, x = 0, y = 0, z = 0, A = "even", B = 0; B < m.length; B++) {
              var C = m[B];
              "even" == A ? (C > y && (y = C, 0 === B && (w = y)), w > C && (w = C), A = "odd") : (C > z && (z = C, 1 == B && (x = z)), x > C && (x = C), A = "even")
            }
            k.origin.size.height = z - x, k.origin.size.width = y - w, k.origin.windowOffset.left += w, k.origin.windowOffset.top += x
        }
      }
      var D = function (a) {
        k.origin.size.height = a.height, k.origin.windowOffset.left = a.left, k.origin.windowOffset.top = a.top, k.origin.size.width = a.width
      };
      for (b._trigger({
        type: "geometry",
        edit: D,
        geometry: {
          height: k.origin.size.height,
          left: k.origin.windowOffset.left,
          top: k.origin.windowOffset.top,
          width: k.origin.size.width
        }
      }), k.origin.windowOffset.right = k.origin.windowOffset.left + k.origin.size.width, k.origin.windowOffset.bottom = k.origin.windowOffset.top + k.origin.size.height, k.origin.offset.left = k.origin.windowOffset.left + k.window.scroll.left, k.origin.offset.top = k.origin.windowOffset.top + k.window.scroll.top, k.origin.offset.bottom = k.origin.offset.top + k.origin.size.height, k.origin.offset.right = k.origin.offset.left + k.origin.size.width, k.available.document = {
        bottom: {
          height: k.document.size.height - k.origin.offset.bottom,
          width: k.document.size.width
        },
        left: {height: k.document.size.height, width: k.origin.offset.left},
        right: {height: k.document.size.height, width: k.document.size.width - k.origin.offset.right},
        top: {height: k.origin.offset.top, width: k.document.size.width}
      }, k.available.window = {
        bottom: {
          height: Math.max(k.window.size.height - Math.max(k.origin.windowOffset.bottom, 0), 0),
          width: k.window.size.width
        },
        left: {height: k.window.size.height, width: Math.max(k.origin.windowOffset.left, 0)},
        right: {
          height: k.window.size.height,
          width: Math.max(k.window.size.width - Math.max(k.origin.windowOffset.right, 0), 0)
        },
        top: {height: Math.max(k.origin.windowOffset.top, 0), width: k.window.size.width}
      }; "html" != j[0].tagName.toLowerCase();) {
        if ("fixed" == j.css("position")) {
          k.origin.fixedLineage = !0;
          break
        }
        j = j.parent()
      }
      return k
    }, __optionsFormat: function () {
      return "number" == typeof this.__options.animationDuration && (this.__options.animationDuration = [this.__options.animationDuration, this.__options.animationDuration]), "number" == typeof this.__options.delay && (this.__options.delay = [this.__options.delay, this.__options.delay]), "number" == typeof this.__options.delayTouch && (this.__options.delayTouch = [this.__options.delayTouch, this.__options.delayTouch]), "string" == typeof this.__options.theme && (this.__options.theme = [this.__options.theme]), "string" == typeof this.__options.parent && (this.__options.parent = a(this.__options.parent)), "hover" == this.__options.trigger ? (this.__options.triggerOpen = {
        mouseenter: !0,
        touchstart: !0
      }, this.__options.triggerClose = {
        mouseleave: !0,
        originClick: !0,
        touchleave: !0
      }) : "click" == this.__options.trigger && (this.__options.triggerOpen = {
        click: !0,
        tap: !0
      }, this.__options.triggerClose = {click: !0, tap: !0}), this._trigger("options"), this
    }, __prepareGC: function () {
      var b = this;
      return b.__options.selfDestruction ? b.__garbageCollector = setInterval(function () {
        var c = (new Date).getTime();
        b.__touchEvents = a.grep(b.__touchEvents, function (a, b) {
          return c - a.time > 6e4
        }), d(b._$origin) || b.destroy()
      }, 2e4) : clearInterval(b.__garbageCollector), b
    }, __prepareOrigin: function () {
      var a = this;
      if (a._$origin.off("." + a.__namespace + "-triggerOpen"), h.hasTouchCapability && a._$origin.on("touchstart." + a.__namespace + "-triggerOpen touchend." + a.__namespace + "-triggerOpen touchcancel." + a.__namespace + "-triggerOpen", function (b) {
          a._touchRecordEvent(b)
        }), a.__options.triggerOpen.click || a.__options.triggerOpen.tap && h.hasTouchCapability) {
        var b = "";
        a.__options.triggerOpen.click && (b += "click." + a.__namespace + "-triggerOpen "), a.__options.triggerOpen.tap && h.hasTouchCapability && (b += "touchend." + a.__namespace + "-triggerOpen"), a._$origin.on(b, function (b) {
          a._touchIsMeaningfulEvent(b) && a._open(b)
        })
      }
      if (a.__options.triggerOpen.mouseenter || a.__options.triggerOpen.touchstart && h.hasTouchCapability) {
        var b = "";
        a.__options.triggerOpen.mouseenter && (b += "mouseenter." + a.__namespace + "-triggerOpen "), a.__options.triggerOpen.touchstart && h.hasTouchCapability && (b += "touchstart." + a.__namespace + "-triggerOpen"), a._$origin.on(b, function (b) {
          !a._touchIsTouchEvent(b) && a._touchIsEmulatedEvent(b) || (a.__pointerIsOverOrigin = !0, a._openShortly(b))
        })
      }
      if (a.__options.triggerClose.mouseleave || a.__options.triggerClose.touchleave && h.hasTouchCapability) {
        var b = "";
        a.__options.triggerClose.mouseleave && (b += "mouseleave." + a.__namespace + "-triggerOpen "), a.__options.triggerClose.touchleave && h.hasTouchCapability && (b += "touchend." + a.__namespace + "-triggerOpen touchcancel." + a.__namespace + "-triggerOpen"), a._$origin.on(b, function (b) {
          a._touchIsMeaningfulEvent(b) && (a.__pointerIsOverOrigin = !1)
        })
      }
      return a
    }, __prepareTooltip: function () {
      var b = this, c = b.__options.interactive ? "auto" : "";
      return b._$tooltip.attr("id", b.__namespace).css({
        "pointer-events": c,
        zIndex: b.__options.zIndex
      }), a.each(b.__previousThemes, function (a, c) {
        b._$tooltip.removeClass(c)
      }), a.each(b.__options.theme, function (a, c) {
        b._$tooltip.addClass(c)
      }), b.__previousThemes = a.merge([], b.__options.theme), b
    }, __scrollHandler: function (b) {
      var c = this;
      if (c.__options.triggerClose.scroll) c._close(b); else {
        if (b.target === h.window.document) c.__Geometry.origin.fixedLineage || c.__options.repositionOnScroll && c.reposition(b); else {
          var d = c.__geometry(), e = !1;
          if ("fixed" != c._$origin.css("position") && c.__$originParents.each(function (b, c) {
              var f = a(c), g = f.css("overflow-x"), h = f.css("overflow-y");
              if ("visible" != g || "visible" != h) {
                var i = c.getBoundingClientRect();
                if ("visible" != g && (d.origin.windowOffset.left < i.left || d.origin.windowOffset.right > i.right)) return e = !0, !1;
                if ("visible" != h && (d.origin.windowOffset.top < i.top || d.origin.windowOffset.bottom > i.bottom)) return e = !0, !1
              }
              return "fixed" == f.css("position") ? !1 : void 0
            }), e) c._$tooltip.css("visibility", "hidden"); else if (c._$tooltip.css("visibility", "visible"), c.__options.repositionOnScroll) c.reposition(b); else {
            var f = d.origin.offset.left - c.__Geometry.origin.offset.left,
              g = d.origin.offset.top - c.__Geometry.origin.offset.top;
            c._$tooltip.css({left: c.__lastPosition.coord.left + f, top: c.__lastPosition.coord.top + g})
          }
        }
        c._trigger({type: "scroll", event: b})
      }
      return c
    }, __stateSet: function (a) {
      return this.__state = a, this._trigger({type: "state", state: a}), this
    }, __timeoutsClear: function () {
      return clearTimeout(this.__timeouts.open), this.__timeouts.open = null, a.each(this.__timeouts.close, function (a, b) {
        clearTimeout(b)
      }), this.__timeouts.close = [], this
    }, __trackerStart: function () {
      var a = this, b = a._$tooltip.find(".tooltipster-content");
      return a.__options.trackTooltip && (a.__contentBcr = b[0].getBoundingClientRect()), a.__tracker = setInterval(function () {
        if (d(a._$origin) && d(a._$tooltip)) {
          if (a.__options.trackOrigin) {
            var e = a.__geometry(), f = !1;
            c(e.origin.size, a.__Geometry.origin.size) && (a.__Geometry.origin.fixedLineage ? c(e.origin.windowOffset, a.__Geometry.origin.windowOffset) && (f = !0) : c(e.origin.offset, a.__Geometry.origin.offset) && (f = !0)), f || (a.__options.triggerClose.mouseleave ? a._close() : a.reposition())
          }
          if (a.__options.trackTooltip) {
            var g = b[0].getBoundingClientRect();
            g.height === a.__contentBcr.height && g.width === a.__contentBcr.width || (a.reposition(), a.__contentBcr = g)
          }
        } else a._close()
      }, a.__options.trackerInterval), a
    }, _close: function (b, c) {
      var d = this, e = !0;
      if (d._trigger({
          type: "close", event: b, stop: function () {
            e = !1
          }
        }), e || d.__destroying) {
        c && d.__callbacks.close.push(c), d.__callbacks.open = [], d.__timeoutsClear();
        var f = function () {
          a.each(d.__callbacks.close, function (a, c) {
            c.call(d, d, {event: b, origin: d._$origin[0]})
          }), d.__callbacks.close = []
        };
        if ("closed" != d.__state) {
          var g = !0, i = new Date, j = i.getTime(), k = j + d.__options.animationDuration[1];
          if ("disappearing" == d.__state && k > d.__closingTime && (g = !1), g) {
            d.__closingTime = k, "disappearing" != d.__state && d.__stateSet("disappearing");
            var l = function () {
              clearInterval(d.__tracker), d._trigger({
                type: "closing",
                event: b
              }), d._$tooltip.off("." + d.__namespace + "-triggerClose").removeClass("tooltipster-dying"), a(h.window).off("." + d.__namespace + "-triggerClose"), d.__$originParents.each(function (b, c) {
                a(c).off("scroll." + d.__namespace + "-triggerClose")
              }), d.__$originParents = null, a("body").off("." + d.__namespace + "-triggerClose"), d._$origin.off("." + d.__namespace + "-triggerClose"), d._off("dismissable"), d.__stateSet("closed"), d._trigger({
                type: "after",
                event: b
              }), d.__options.functionAfter && d.__options.functionAfter.call(d, d, {
                event: b,
                origin: d._$origin[0]
              }), f()
            };
            h.hasTransitions ? (d._$tooltip.css({
              "-moz-animation-duration": d.__options.animationDuration[1] + "ms",
              "-ms-animation-duration": d.__options.animationDuration[1] + "ms",
              "-o-animation-duration": d.__options.animationDuration[1] + "ms",
              "-webkit-animation-duration": d.__options.animationDuration[1] + "ms",
              "animation-duration": d.__options.animationDuration[1] + "ms",
              "transition-duration": d.__options.animationDuration[1] + "ms"
            }), d._$tooltip.clearQueue().removeClass("tooltipster-show").addClass("tooltipster-dying"), d.__options.animationDuration[1] > 0 && d._$tooltip.delay(d.__options.animationDuration[1]), d._$tooltip.queue(l)) : d._$tooltip.stop().fadeOut(d.__options.animationDuration[1], l)
          }
        } else f()
      }
      return d
    }, _off: function () {
      return this.__$emitterPrivate.off.apply(this.__$emitterPrivate, Array.prototype.slice.apply(arguments)), this
    }, _on: function () {
      return this.__$emitterPrivate.on.apply(this.__$emitterPrivate, Array.prototype.slice.apply(arguments)), this
    }, _one: function () {
      return this.__$emitterPrivate.one.apply(this.__$emitterPrivate, Array.prototype.slice.apply(arguments)), this
    }, _open: function (b, c) {
      var e = this;
      if (!e.__destroying && d(e._$origin) && e.__enabled) {
        var f = !0;
        if ("closed" == e.__state && (e._trigger({
            type: "before", event: b, stop: function () {
              f = !1
            }
          }), f && e.__options.functionBefore && (f = e.__options.functionBefore.call(e, e, {
            event: b,
            origin: e._$origin[0]
          }))), f !== !1 && null !== e.__Content) {
          c && e.__callbacks.open.push(c), e.__callbacks.close = [], e.__timeoutsClear();
          var g, i = function () {
            "stable" != e.__state && e.__stateSet("stable"), a.each(e.__callbacks.open, function (a, b) {
              b.call(e, e, {origin: e._$origin[0], tooltip: e._$tooltip[0]})
            }), e.__callbacks.open = []
          };
          if ("closed" !== e.__state) g = 0, "disappearing" === e.__state ? (e.__stateSet("appearing"), h.hasTransitions ? (e._$tooltip.clearQueue().removeClass("tooltipster-dying").addClass("tooltipster-show"), e.__options.animationDuration[0] > 0 && e._$tooltip.delay(e.__options.animationDuration[0]), e._$tooltip.queue(i)) : e._$tooltip.stop().fadeIn(i)) : "stable" == e.__state && i(); else {
            if (e.__stateSet("appearing"), g = e.__options.animationDuration[0], e.__contentInsert(), e.reposition(b, !0), h.hasTransitions ? (e._$tooltip.addClass("tooltipster-" + e.__options.animation).addClass("tooltipster-initial").css({
                "-moz-animation-duration": e.__options.animationDuration[0] + "ms",
                "-ms-animation-duration": e.__options.animationDuration[0] + "ms",
                "-o-animation-duration": e.__options.animationDuration[0] + "ms",
                "-webkit-animation-duration": e.__options.animationDuration[0] + "ms",
                "animation-duration": e.__options.animationDuration[0] + "ms",
                "transition-duration": e.__options.animationDuration[0] + "ms"
              }), setTimeout(function () {
                "closed" != e.__state && (e._$tooltip.addClass("tooltipster-show").removeClass("tooltipster-initial"), e.__options.animationDuration[0] > 0 && e._$tooltip.delay(e.__options.animationDuration[0]), e._$tooltip.queue(i))
              }, 0)) : e._$tooltip.css("display", "none").fadeIn(e.__options.animationDuration[0], i), e.__trackerStart(), a(h.window).on("resize." + e.__namespace + "-triggerClose", function (b) {
                var c = a(document.activeElement);
                (c.is("input") || c.is("textarea")) && a.contains(e._$tooltip[0], c[0]) || e.reposition(b)
              }).on("scroll." + e.__namespace + "-triggerClose", function (a) {
                e.__scrollHandler(a)
              }), e.__$originParents = e._$origin.parents(), e.__$originParents.each(function (b, c) {
                a(c).on("scroll." + e.__namespace + "-triggerClose", function (a) {
                  e.__scrollHandler(a)
                })
              }), e.__options.triggerClose.mouseleave || e.__options.triggerClose.touchleave && h.hasTouchCapability) {
              e._on("dismissable", function (a) {
                a.dismissable ? a.delay ? (m = setTimeout(function () {
                  e._close(a.event)
                }, a.delay), e.__timeouts.close.push(m)) : e._close(a) : clearTimeout(m)
              });
              var j = e._$origin, k = "", l = "", m = null;
              e.__options.interactive && (j = j.add(e._$tooltip)), e.__options.triggerClose.mouseleave && (k += "mouseenter." + e.__namespace + "-triggerClose ", l += "mouseleave." + e.__namespace + "-triggerClose "), e.__options.triggerClose.touchleave && h.hasTouchCapability && (k += "touchstart." + e.__namespace + "-triggerClose", l += "touchend." + e.__namespace + "-triggerClose touchcancel." + e.__namespace + "-triggerClose"), j.on(l, function (a) {
                if (e._touchIsTouchEvent(a) || !e._touchIsEmulatedEvent(a)) {
                  var b = "mouseleave" == a.type ? e.__options.delay : e.__options.delayTouch;
                  e._trigger({delay: b[1], dismissable: !0, event: a, type: "dismissable"})
                }
              }).on(k, function (a) {
                !e._touchIsTouchEvent(a) && e._touchIsEmulatedEvent(a) || e._trigger({
                  dismissable: !1,
                  event: a,
                  type: "dismissable"
                })
              })
            }
            e.__options.triggerClose.originClick && e._$origin.on("click." + e.__namespace + "-triggerClose", function (a) {
              e._touchIsTouchEvent(a) || e._touchIsEmulatedEvent(a) || e._close(a)
            }), (e.__options.triggerClose.click || e.__options.triggerClose.tap && h.hasTouchCapability) && setTimeout(function () {
              if ("closed" != e.__state) {
                var b = "";
                e.__options.triggerClose.click && (b += "click." + e.__namespace + "-triggerClose "), e.__options.triggerClose.tap && h.hasTouchCapability && (b += "touchend." + e.__namespace + "-triggerClose"), a("body").on(b, function (b) {
                  e._touchIsMeaningfulEvent(b) && (e._touchRecordEvent(b), e.__options.interactive && a.contains(e._$tooltip[0], b.target) || e._close(b))
                }), e.__options.triggerClose.tap && h.hasTouchCapability && a("body").on("touchstart." + e.__namespace + "-triggerClose", function (a) {
                  e._touchRecordEvent(a)
                })
              }
            }, 0), e._trigger("ready"), e.__options.functionReady && e.__options.functionReady.call(e, e, {
              origin: e._$origin[0],
              tooltip: e._$tooltip[0]
            })
          }
          if (e.__options.timer > 0) {
            var m = setTimeout(function () {
              e._close()
            }, e.__options.timer + g);
            e.__timeouts.close.push(m)
          }
        }
      }
      return e
    }, _openShortly: function (a) {
      var b = this, c = !0;
      if ("stable" != b.__state && "appearing" != b.__state && !b.__timeouts.open && (b._trigger({
          type: "start",
          event: a,
          stop: function () {
            c = !1
          }
        }), c)) {
        var d = 0 == a.type.indexOf("touch") ? b.__options.delayTouch : b.__options.delay;
        d[0] ? b.__timeouts.open = setTimeout(function () {
          b.__timeouts.open = null, b.__pointerIsOverOrigin && b._touchIsMeaningfulEvent(a) ? (b._trigger("startend"), b._open(a)) : b._trigger("startcancel")
        }, d[0]) : (b._trigger("startend"), b._open(a))
      }
      return b
    }, _optionsExtract: function (b, c) {
      var d = this, e = a.extend(!0, {}, c), f = d.__options[b];
      return f || (f = {}, a.each(c, function (a, b) {
        var c = d.__options[a];
        void 0 !== c && (f[a] = c)
      })), a.each(e, function (b, c) {
        void 0 !== f[b] && ("object" != typeof c || c instanceof Array || null == c || "object" != typeof f[b] || f[b] instanceof Array || null == f[b] ? e[b] = f[b] : a.extend(e[b], f[b]))
      }), e
    }, _plug: function (b) {
      var c = a.tooltipster._plugin(b);
      if (!c) throw new Error('The "' + b + '" plugin is not defined');
      return c.instance && a.tooltipster.__bridge(c.instance, this, c.name), this
    }, _touchIsEmulatedEvent: function (a) {
      for (var b = !1, c = (new Date).getTime(), d = this.__touchEvents.length - 1; d >= 0; d--) {
        var e = this.__touchEvents[d];
        if (!(c - e.time < 500)) break;
        e.target === a.target && (b = !0)
      }
      return b
    }, _touchIsMeaningfulEvent: function (a) {
      return this._touchIsTouchEvent(a) && !this._touchSwiped(a.target) || !this._touchIsTouchEvent(a) && !this._touchIsEmulatedEvent(a)
    }, _touchIsTouchEvent: function (a) {
      return 0 == a.type.indexOf("touch")
    }, _touchRecordEvent: function (a) {
      return this._touchIsTouchEvent(a) && (a.time = (new Date).getTime(), this.__touchEvents.push(a)), this
    }, _touchSwiped: function (a) {
      for (var b = !1, c = this.__touchEvents.length - 1; c >= 0; c--) {
        var d = this.__touchEvents[c];
        if ("touchmove" == d.type) {
          b = !0;
          break
        }
        if ("touchstart" == d.type && a === d.target) break
      }
      return b
    }, _trigger: function () {
      var b = Array.prototype.slice.apply(arguments);
      return "string" == typeof b[0] && (b[0] = {type: b[0]}), b[0].instance = this, b[0].origin = this._$origin ? this._$origin[0] : null, b[0].tooltip = this._$tooltip ? this._$tooltip[0] : null, this.__$emitterPrivate.trigger.apply(this.__$emitterPrivate, b), a.tooltipster._trigger.apply(a.tooltipster, b), this.__$emitterPublic.trigger.apply(this.__$emitterPublic, b), this
    }, _unplug: function (b) {
      var c = this;
      if (c[b]) {
        var d = a.tooltipster._plugin(b);
        d.instance && a.each(d.instance, function (a, d) {
          c[a] && c[a].bridged === c[b] && delete c[a]
        }), c[b].__destroy && c[b].__destroy(), delete c[b]
      }
      return c
    }, close: function (a) {
      return this.__destroyed ? this.__destroyError() : this._close(null, a), this
    }, content: function (a) {
      var b = this;
      if (void 0 === a) return b.__Content;
      if (b.__destroyed) b.__destroyError(); else if (b.__contentSet(a), null !== b.__Content) {
        if ("closed" !== b.__state && (b.__contentInsert(), b.reposition(), b.__options.updateAnimation)) if (h.hasTransitions) {
          var c = b.__options.updateAnimation;
          b._$tooltip.addClass("tooltipster-update-" + c), setTimeout(function () {
            "closed" != b.__state && b._$tooltip.removeClass("tooltipster-update-" + c)
          }, 1e3)
        } else b._$tooltip.fadeTo(200, .5, function () {
          "closed" != b.__state && b._$tooltip.fadeTo(200, 1)
        })
      } else b._close();
      return b
    }, destroy: function () {
      var b = this;
      return b.__destroyed ? b.__destroyError() : b.__destroying || (b.__destroying = !0, b._close(null, function () {
        b._trigger("destroy"), b.__destroying = !1, b.__destroyed = !0, b._$origin.removeData(b.__namespace).off("." + b.__namespace + "-triggerOpen"), a("body").off("." + b.__namespace + "-triggerOpen");
        var c = b._$origin.data("tooltipster-ns");
        if (c) if (1 === c.length) {
          var d = null;
          "previous" == b.__options.restoration ? d = b._$origin.data("tooltipster-initialTitle") : "current" == b.__options.restoration && (d = "string" == typeof b.__Content ? b.__Content : a("<div></div>").append(b.__Content).html()), d && b._$origin.attr("title", d), b._$origin.removeClass("tooltipstered"), b._$origin.removeData("tooltipster-ns").removeData("tooltipster-initialTitle")
        } else c = a.grep(c, function (a, c) {
          return a !== b.__namespace
        }), b._$origin.data("tooltipster-ns", c);
        b._trigger("destroyed"), b._off(), b.off(), b.__Content = null, b.__$emitterPrivate = null, b.__$emitterPublic = null, b.__options.parent = null, b._$origin = null, b._$tooltip = null, a.tooltipster.__instancesLatestArr = a.grep(a.tooltipster.__instancesLatestArr, function (a, c) {
          return b !== a
        }), clearInterval(b.__garbageCollector)
      })), b
    }, disable: function () {
      return this.__destroyed ? (this.__destroyError(), this) : (this._close(), this.__enabled = !1, this)
    }, elementOrigin: function () {
      return this.__destroyed ? void this.__destroyError() : this._$origin[0]
    }, elementTooltip: function () {
      return this._$tooltip ? this._$tooltip[0] : null
    }, enable: function () {
      return this.__enabled = !0, this
    }, hide: function (a) {
      return this.close(a)
    }, instance: function () {
      return this
    }, off: function () {
      return this.__destroyed || this.__$emitterPublic.off.apply(this.__$emitterPublic, Array.prototype.slice.apply(arguments)), this
    }, on: function () {
      return this.__destroyed ? this.__destroyError() : this.__$emitterPublic.on.apply(this.__$emitterPublic, Array.prototype.slice.apply(arguments)), this
    }, one: function () {
      return this.__destroyed ? this.__destroyError() : this.__$emitterPublic.one.apply(this.__$emitterPublic, Array.prototype.slice.apply(arguments)), this
    }, open: function (a) {
      return this.__destroyed || this.__destroying ? this.__destroyError() : this._open(null, a), this
    }, option: function (b, c) {
      return void 0 === c ? this.__options[b] : (this.__destroyed ? this.__destroyError() : (this.__options[b] = c, this.__optionsFormat(), a.inArray(b, ["trigger", "triggerClose", "triggerOpen"]) >= 0 && this.__prepareOrigin(), "selfDestruction" === b && this.__prepareGC()), this)
    }, reposition: function (a, b) {
      var c = this;
      return c.__destroyed ? c.__destroyError() : "closed" != c.__state && d(c._$origin) && (b || d(c._$tooltip)) && (b || c._$tooltip.detach(), c.__Geometry = c.__geometry(), c._trigger({
        type: "reposition",
        event: a,
        helper: {geo: c.__Geometry}
      })), c
    }, show: function (a) {
      return this.open(a)
    }, status: function () {
      return {
        destroyed: this.__destroyed,
        destroying: this.__destroying,
        enabled: this.__enabled,
        open: "closed" !== this.__state,
        state: this.__state
      }
    }, triggerHandler: function () {
      return this.__destroyed ? this.__destroyError() : this.__$emitterPublic.triggerHandler.apply(this.__$emitterPublic, Array.prototype.slice.apply(arguments)), this
    }
  }, a.fn.tooltipster = function () {
    var b = Array.prototype.slice.apply(arguments),
      c = "You are using a single HTML element as content for several tooltips. You probably want to set the contentCloning option to TRUE.";
    if (0 === this.length) return this;
    if ("string" == typeof b[0]) {
      var d = "#*$~&";
      return this.each(function () {
        var e = a(this).data("tooltipster-ns"), f = e ? a(this).data(e[0]) : null;
        if (!f) throw new Error("You called Tooltipster's \"" + b[0] + '" method on an uninitialized element');
        if ("function" != typeof f[b[0]]) throw new Error('Unknown method "' + b[0] + '"');
        this.length > 1 && "content" == b[0] && (b[1] instanceof a || "object" == typeof b[1] && null != b[1] && b[1].tagName) && !f.__options.contentCloning && f.__options.debug && console.log(c);
        var g = f[b[0]](b[1], b[2]);
        return g !== f || "instance" === b[0] ? (d = g, !1) : void 0
      }), "#*$~&" !== d ? d : this
    }
    a.tooltipster.__instancesLatestArr = [];
    var e = b[0] && void 0 !== b[0].multiple, g = e && b[0].multiple || !e && f.multiple,
      h = b[0] && void 0 !== b[0].content, i = h && b[0].content || !h && f.content,
      j = b[0] && void 0 !== b[0].contentCloning, k = j && b[0].contentCloning || !j && f.contentCloning,
      l = b[0] && void 0 !== b[0].debug, m = l && b[0].debug || !l && f.debug;
    return this.length > 1 && (i instanceof a || "object" == typeof i && null != i && i.tagName) && !k && m && console.log(c), this.each(function () {
      var c = !1, d = a(this), e = d.data("tooltipster-ns"), f = null;
      e ? g ? c = !0 : m && (console.log("Tooltipster: one or more tooltips are already attached to the element below. Ignoring."), console.log(this)) : c = !0, c && (f = new a.Tooltipster(this, b[0]), e || (e = []), e.push(f.__namespace), d.data("tooltipster-ns", e), d.data(f.__namespace, f), f.__options.functionInit && f.__options.functionInit.call(f, f, {origin: this}), f._trigger("init")), a.tooltipster.__instancesLatestArr.push(f)
    }), this
  }, b.prototype = {
    __init: function (b) {
      this.__$tooltip = b, this.__$tooltip.css({
        left: 0,
        overflow: "hidden",
        position: "absolute",
        top: 0
      }).find(".tooltipster-content").css("overflow", "auto"), this.$container = a('<div class="tooltipster-ruler"></div>').append(this.__$tooltip).appendTo("body")
    }, __forceRedraw: function () {
      var a = this.__$tooltip.parent();
      this.__$tooltip.detach(), this.__$tooltip.appendTo(a)
    }, constrain: function (a, b) {
      return this.constraints = {width: a, height: b}, this.__$tooltip.css({
        display: "block",
        height: "",
        overflow: "auto",
        width: a
      }), this
    }, destroy: function () {
      this.__$tooltip.detach().find(".tooltipster-content").css({display: "", overflow: ""}), this.$container.remove()
    }, free: function () {
      return this.constraints = null, this.__$tooltip.css({
        display: "",
        height: "",
        overflow: "visible",
        width: ""
      }), this
    }, measure: function () {
      this.__forceRedraw();
      var a = this.__$tooltip[0].getBoundingClientRect(),
        b = {size: {height: a.height || a.bottom, width: a.width || a.right}};
      if (this.constraints) {
        var c = this.__$tooltip.find(".tooltipster-content"), d = this.__$tooltip.outerHeight(),
          e = c[0].getBoundingClientRect(), f = {
            height: d <= this.constraints.height,
            width: a.width <= this.constraints.width && e.width >= c[0].scrollWidth - 1
          };
        b.fits = f.height && f.width
      }
      return h.IE && h.IE <= 11 && b.size.width !== h.window.document.documentElement.clientWidth && (b.size.width = Math.ceil(b.size.width) + 1), b
    }
  };
  var j = navigator.userAgent.toLowerCase();
  -1 != j.indexOf("msie") ? h.IE = parseInt(j.split("msie")[1]) : -1 !== j.toLowerCase().indexOf("trident") && -1 !== j.indexOf(" rv:11") ? h.IE = 11 : -1 != j.toLowerCase().indexOf("edge/") && (h.IE = parseInt(j.toLowerCase().split("edge/")[1]));
  var k = "tooltipster.sideTip";
  return a.tooltipster._plugin({
    name: k, instance: {
      __defaults: function () {
        return {
          arrow: !0,
          distance: 6,
          functionPosition: null,
          maxWidth: null,
          minIntersection: 16,
          minWidth: 0,
          position: null,
          side: "top",
          viewportAware: !0
        }
      }, __init: function (a) {
        var b = this;
        b.__instance = a, b.__namespace = "tooltipster-sideTip-" + Math.round(1e6 * Math.random()), b.__previousState = "closed", b.__options, b.__optionsFormat(), b.__instance._on("state." + b.__namespace, function (a) {
          "closed" == a.state ? b.__close() : "appearing" == a.state && "closed" == b.__previousState && b.__create(), b.__previousState = a.state
        }), b.__instance._on("options." + b.__namespace, function () {
          b.__optionsFormat()
        }), b.__instance._on("reposition." + b.__namespace, function (a) {
          b.__reposition(a.event, a.helper)
        })
      }, __close: function () {
        this.__instance.content() instanceof a && this.__instance.content().detach(), this.__instance._$tooltip.remove(), this.__instance._$tooltip = null
      }, __create: function () {
        var b = a('<div class="tooltipster-base tooltipster-sidetip"><div class="tooltipster-box"><div class="tooltipster-content"></div></div><div class="tooltipster-arrow"><div class="tooltipster-arrow-uncropped"><div class="tooltipster-arrow-border"></div><div class="tooltipster-arrow-background"></div></div></div></div>');
        this.__options.arrow || b.find(".tooltipster-box").css("margin", 0).end().find(".tooltipster-arrow").hide(), this.__options.minWidth && b.css("min-width", this.__options.minWidth + "px"), this.__options.maxWidth && b.css("max-width", this.__options.maxWidth + "px"), this.__instance._$tooltip = b, this.__instance._trigger("created")
      }, __destroy: function () {
        this.__instance._off("." + self.__namespace)
      }, __optionsFormat: function () {
        var b = this;
        if (b.__options = b.__instance._optionsExtract(k, b.__defaults()),
          b.__options.position && (b.__options.side = b.__options.position), "object" != typeof b.__options.distance && (b.__options.distance = [b.__options.distance]), b.__options.distance.length < 4 && (void 0 === b.__options.distance[1] && (b.__options.distance[1] = b.__options.distance[0]), void 0 === b.__options.distance[2] && (b.__options.distance[2] = b.__options.distance[0]), void 0 === b.__options.distance[3] && (b.__options.distance[3] = b.__options.distance[1]), b.__options.distance = {
            top: b.__options.distance[0],
            right: b.__options.distance[1],
            bottom: b.__options.distance[2],
            left: b.__options.distance[3]
          }), "string" == typeof b.__options.side) {
          var c = {top: "bottom", right: "left", bottom: "top", left: "right"};
          b.__options.side = [b.__options.side, c[b.__options.side]], "left" == b.__options.side[0] || "right" == b.__options.side[0] ? b.__options.side.push("top", "bottom") : b.__options.side.push("right", "left")
        }
        6 === a.tooltipster._env.IE && b.__options.arrow !== !0 && (b.__options.arrow = !1)
      }, __reposition: function (b, c) {
        var d, e = this, f = e.__targetFind(c), g = [];
        e.__instance._$tooltip.detach();
        var h = e.__instance._$tooltip.clone(), i = a.tooltipster._getRuler(h), j = !1,
          k = e.__instance.option("animation");
        switch (k && h.removeClass("tooltipster-" + k), a.each(["window", "document"], function (d, k) {
          var l = null;
          if (e.__instance._trigger({
              container: k, helper: c, satisfied: j, takeTest: function (a) {
                l = a
              }, results: g, type: "positionTest"
            }), 1 == l || 0 != l && 0 == j && ("window" != k || e.__options.viewportAware)) for (var d = 0; d < e.__options.side.length; d++) {
            var m = {horizontal: 0, vertical: 0}, n = e.__options.side[d];
            "top" == n || "bottom" == n ? m.vertical = e.__options.distance[n] : m.horizontal = e.__options.distance[n], e.__sideChange(h, n), a.each(["natural", "constrained"], function (a, d) {
              if (l = null, e.__instance._trigger({
                  container: k,
                  event: b,
                  helper: c,
                  mode: d,
                  results: g,
                  satisfied: j,
                  side: n,
                  takeTest: function (a) {
                    l = a
                  },
                  type: "positionTest"
                }), 1 == l || 0 != l && 0 == j) {
                var h = {
                    container: k,
                    distance: m,
                    fits: null,
                    mode: d,
                    outerSize: null,
                    side: n,
                    size: null,
                    target: f[n],
                    whole: null
                  },
                  o = "natural" == d ? i.free() : i.constrain(c.geo.available[k][n].width - m.horizontal, c.geo.available[k][n].height - m.vertical),
                  p = o.measure();
                if (h.size = p.size, h.outerSize = {
                    height: p.size.height + m.vertical,
                    width: p.size.width + m.horizontal
                  }, "natural" == d ? c.geo.available[k][n].width >= h.outerSize.width && c.geo.available[k][n].height >= h.outerSize.height ? h.fits = !0 : h.fits = !1 : h.fits = p.fits, "window" == k && (h.fits ? "top" == n || "bottom" == n ? h.whole = c.geo.origin.windowOffset.right >= e.__options.minIntersection && c.geo.window.size.width - c.geo.origin.windowOffset.left >= e.__options.minIntersection : h.whole = c.geo.origin.windowOffset.bottom >= e.__options.minIntersection && c.geo.window.size.height - c.geo.origin.windowOffset.top >= e.__options.minIntersection : h.whole = !1), g.push(h), h.whole) j = !0; else if ("natural" == h.mode && (h.fits || h.size.width <= c.geo.available[k][n].width)) return !1
              }
            })
          }
        }), e.__instance._trigger({
          edit: function (a) {
            g = a
          }, event: b, helper: c, results: g, type: "positionTested"
        }), g.sort(function (a, b) {
          if (a.whole && !b.whole) return -1;
          if (!a.whole && b.whole) return 1;
          if (a.whole && b.whole) {
            var c = e.__options.side.indexOf(a.side), d = e.__options.side.indexOf(b.side);
            return d > c ? -1 : c > d ? 1 : "natural" == a.mode ? -1 : 1
          }
          if (a.fits && !b.fits) return -1;
          if (!a.fits && b.fits) return 1;
          if (a.fits && b.fits) {
            var c = e.__options.side.indexOf(a.side), d = e.__options.side.indexOf(b.side);
            return d > c ? -1 : c > d ? 1 : "natural" == a.mode ? -1 : 1
          }
          return "document" == a.container && "bottom" == a.side && "natural" == a.mode ? -1 : 1
        }), d = g[0], d.coord = {}, d.side) {
          case"left":
          case"right":
            d.coord.top = Math.floor(d.target - d.size.height / 2);
            break;
          case"bottom":
          case"top":
            d.coord.left = Math.floor(d.target - d.size.width / 2)
        }
        switch (d.side) {
          case"left":
            d.coord.left = c.geo.origin.windowOffset.left - d.outerSize.width;
            break;
          case"right":
            d.coord.left = c.geo.origin.windowOffset.right + d.distance.horizontal;
            break;
          case"top":
            d.coord.top = c.geo.origin.windowOffset.top - d.outerSize.height;
            break;
          case"bottom":
            d.coord.top = c.geo.origin.windowOffset.bottom + d.distance.vertical
        }
        "window" == d.container ? "top" == d.side || "bottom" == d.side ? d.coord.left < 0 ? c.geo.origin.windowOffset.right - this.__options.minIntersection >= 0 ? d.coord.left = 0 : d.coord.left = c.geo.origin.windowOffset.right - this.__options.minIntersection - 1 : d.coord.left > c.geo.window.size.width - d.size.width && (c.geo.origin.windowOffset.left + this.__options.minIntersection <= c.geo.window.size.width ? d.coord.left = c.geo.window.size.width - d.size.width : d.coord.left = c.geo.origin.windowOffset.left + this.__options.minIntersection + 1 - d.size.width) : d.coord.top < 0 ? c.geo.origin.windowOffset.bottom - this.__options.minIntersection >= 0 ? d.coord.top = 0 : d.coord.top = c.geo.origin.windowOffset.bottom - this.__options.minIntersection - 1 : d.coord.top > c.geo.window.size.height - d.size.height && (c.geo.origin.windowOffset.top + this.__options.minIntersection <= c.geo.window.size.height ? d.coord.top = c.geo.window.size.height - d.size.height : d.coord.top = c.geo.origin.windowOffset.top + this.__options.minIntersection + 1 - d.size.height) : (d.coord.left > c.geo.window.size.width - d.size.width && (d.coord.left = c.geo.window.size.width - d.size.width), d.coord.left < 0 && (d.coord.left = 0)), e.__sideChange(h, d.side), c.tooltipClone = h[0], c.tooltipParent = e.__instance.option("parent").parent[0], c.mode = d.mode, c.whole = d.whole, c.origin = e.__instance._$origin[0], c.tooltip = e.__instance._$tooltip[0], delete d.container, delete d.fits, delete d.mode, delete d.outerSize, delete d.whole, d.distance = d.distance.horizontal || d.distance.vertical;
        var l = a.extend(!0, {}, d);
        if (e.__instance._trigger({
            edit: function (a) {
              d = a
            }, event: b, helper: c, position: l, type: "position"
          }), e.__options.functionPosition) {
          var m = e.__options.functionPosition.call(e, e.__instance, c, l);
          m && (d = m)
        }
        i.destroy();
        var n, o;
        "top" == d.side || "bottom" == d.side ? (n = {
          prop: "left",
          val: d.target - d.coord.left
        }, o = d.size.width - this.__options.minIntersection) : (n = {
          prop: "top",
          val: d.target - d.coord.top
        }, o = d.size.height - this.__options.minIntersection), n.val < this.__options.minIntersection ? n.val = this.__options.minIntersection : n.val > o && (n.val = o);
        var p;
        p = c.geo.origin.fixedLineage ? c.geo.origin.windowOffset : {
          left: c.geo.origin.windowOffset.left + c.geo.window.scroll.left,
          top: c.geo.origin.windowOffset.top + c.geo.window.scroll.top
        }, d.coord = {
          left: p.left + (d.coord.left - c.geo.origin.windowOffset.left),
          top: p.top + (d.coord.top - c.geo.origin.windowOffset.top)
        }, e.__sideChange(e.__instance._$tooltip, d.side), c.geo.origin.fixedLineage ? e.__instance._$tooltip.css("position", "fixed") : e.__instance._$tooltip.css("position", ""), e.__instance._$tooltip.css({
          left: d.coord.left,
          top: d.coord.top,
          height: d.size.height,
          width: d.size.width
        }).find(".tooltipster-arrow").css({
          left: "",
          top: ""
        }).css(n.prop, n.val), e.__instance._$tooltip.appendTo(e.__instance.option("parent")), e.__instance._trigger({
          type: "repositioned",
          event: b,
          position: d
        })
      }, __sideChange: function (a, b) {
        a.removeClass("tooltipster-bottom").removeClass("tooltipster-left").removeClass("tooltipster-right").removeClass("tooltipster-top").addClass("tooltipster-" + b)
      }, __targetFind: function (a) {
        var b = {}, c = this.__instance._$origin[0].getClientRects();
        if (c.length > 1) {
          var d = this.__instance._$origin.css("opacity");
          1 == d && (this.__instance._$origin.css("opacity", .99), c = this.__instance._$origin[0].getClientRects(), this.__instance._$origin.css("opacity", 1))
        }
        if (c.length < 2) b.top = Math.floor(a.geo.origin.windowOffset.left + a.geo.origin.size.width / 2), b.bottom = b.top, b.left = Math.floor(a.geo.origin.windowOffset.top + a.geo.origin.size.height / 2), b.right = b.left; else {
          var e = c[0];
          b.top = Math.floor(e.left + (e.right - e.left) / 2), e = c.length > 2 ? c[Math.ceil(c.length / 2) - 1] : c[0], b.right = Math.floor(e.top + (e.bottom - e.top) / 2), e = c[c.length - 1], b.bottom = Math.floor(e.left + (e.right - e.left) / 2), e = c.length > 2 ? c[Math.ceil((c.length + 1) / 2) - 1] : c[c.length - 1], b.left = Math.floor(e.top + (e.bottom - e.top) / 2)
        }
        return b
      }
    }
  }), a
});


/* Stupid table */
(function (c) {
  c.fn.stupidtable = function (a) {
    return this.each(function () {
      var b = c(this);
      a = a || {};
      a = c.extend({}, c.fn.stupidtable.default_sort_fns, a);
      b.data("sortFns", a);
      b.stupidtable_build();
      b.on("click.stupidtable", "thead th", function () {
        c(this).stupidsort()
      });
      b.find("th[data-sort-onload=yes]").eq(0).stupidsort()
    })
  };
  c.fn.stupidtable.default_settings = {
    should_redraw: function (a) {
      return !0
    }, will_manually_build_table: !1
  };
  c.fn.stupidtable.dir = {ASC: "asc", DESC: "desc"};
  c.fn.stupidtable.default_sort_fns = {
    "int": function (a,
                     b) {
      return parseInt(a, 10) - parseInt(b, 10)
    }, "float": function (a, b) {
      return parseFloat(a) - parseFloat(b)
    }, string: function (a, b) {
      return a.toString().localeCompare(b.toString())
    }, "string-ins": function (a, b) {
      a = a.toString().toLocaleLowerCase();
      b = b.toString().toLocaleLowerCase();
      return a.localeCompare(b)
    }
  };
  c.fn.stupidtable_settings = function (a) {
    return this.each(function () {
      var b = c(this), f = c.extend({}, c.fn.stupidtable.default_settings, a);
      b.stupidtable.settings = f
    })
  };
  c.fn.stupidsort = function (a) {
    var b = c(this), f = b.data("sort") ||
      null;
    if (null !== f) {
      var d = b.closest("table"), e = {$th: b, $table: d, datatype: f};
      d.stupidtable.settings || (d.stupidtable.settings = c.extend({}, c.fn.stupidtable.default_settings));
      e.compare_fn = d.data("sortFns")[f];
      e.th_index = h(e);
      e.sort_dir = k(a, e);
      b.data("sort-dir", e.sort_dir);
      d.trigger("beforetablesort", {column: e.th_index, direction: e.sort_dir, $th: b});
      d.css("display");
      setTimeout(function () {
        d.stupidtable.settings.will_manually_build_table || d.stupidtable_build();
        var a = l(e), a = m(a, e);
        if (d.stupidtable.settings.should_redraw(e)) {
          d.children("tbody").append(a);
          var a = e.$table, c = e.$th, f = c.data("sort-dir");
          a.find("th").data("sort-dir", null).removeClass("sorting-desc sorting-asc");
          c.data("sort-dir", f).addClass("sorting-" + f);
          d.trigger("aftertablesort", {column: e.th_index, direction: e.sort_dir, $th: b});
          d.css("display")
        }
      }, 10);
      return b
    }
  };
  c.fn.updateSortVal = function (a) {
    var b = c(this);
    b.is("[data-sort-value]") && b.attr("data-sort-value", a);
    b.data("sort-value", a);
    return b
  };
  c.fn.stupidtable_build = function () {
    return this.each(function () {
      var a = c(this), b = [];
      a.children("tbody").children("tr").each(function (a,
                                                        d) {
        var e = {$tr: c(d), columns: [], index: a};
        c(d).children("td").each(function (a, b) {
          var d = c(b).data("sort-value");
          "undefined" === typeof d && (d = c(b).text(), c(b).data("sort-value", d));
          e.columns.push(d)
        });
        b.push(e)
      });
      a.data("stupidsort_internaltable", b)
    })
  };
  var l = function (a) {
    var b = a.$table.data("stupidsort_internaltable"), f = a.th_index, d = a.$th.data("sort-multicolumn"),
      d = d ? d.split(",") : [], e = c.map(d, function (b, d) {
        var c = a.$table.find("th"), e = parseInt(b, 10), f;
        e || 0 === e ? f = c.eq(e) : (f = c.siblings("#" + b), e = c.index(f));
        return {index: e, $e: f}
      });
    b.sort(function (b, c) {
      for (var d = e.slice(0), g = a.compare_fn(b.columns[f], c.columns[f]); 0 === g && d.length;) {
        var g = d[0], h = g.$e.data("sort"),
          g = (0, a.$table.data("sortFns")[h])(b.columns[g.index], c.columns[g.index]);
        d.shift()
      }
      return 0 === g ? b.index - c.index : g
    });
    a.sort_dir != c.fn.stupidtable.dir.ASC && b.reverse();
    return b
  }, m = function (a, b) {
    var f = c.map(a, function (a, c) {
      return [[a.columns[b.th_index], a.$tr, c]]
    });
    b.column = f;
    return c.map(a, function (a) {
      return a.$tr
    })
  }, k = function (a, b) {
    var f, d = b.$th,
      e = c.fn.stupidtable.dir;
    a ? f = a : (f = a || d.data("sort-default") || e.ASC, d.data("sort-dir") && (f = d.data("sort-dir") === e.ASC ? e.DESC : e.ASC));
    return f
  }, h = function (a) {
    var b = 0, f = a.$th.index();
    a.$th.parents("tr").find("th").slice(0, f).each(function () {
      var a = c(this).attr("colspan") || 1;
      b += parseInt(a, 10)
    });
    return b
  }
})(jQuery);


/* Fast Click */
!function () {
  "use strict";

  function a(b, d) {
    function f(a, b) {
      return function () {
        return a.apply(b, arguments)
      }
    }

    var e;
    if (d = d || {}, this.trackingClick = !1, this.trackingClickStart = 0, this.targetElement = null, this.touchStartX = 0, this.touchStartY = 0, this.lastTouchIdentifier = 0, this.touchBoundary = d.touchBoundary || 10, this.layer = b, this.tapDelay = d.tapDelay || 200, this.tapTimeout = d.tapTimeout || 700, !a.notNeeded(b)) {
      for (var g = ["onMouse", "onClick", "onTouchStart", "onTouchMove", "onTouchEnd", "onTouchCancel"], h = this, i = 0, j = g.length; i < j; i++) h[g[i]] = f(h[g[i]], h);
      c && (b.addEventListener("mouseover", this.onMouse, !0), b.addEventListener("mousedown", this.onMouse, !0), b.addEventListener("mouseup", this.onMouse, !0)), b.addEventListener("click", this.onClick, !0), b.addEventListener("touchstart", this.onTouchStart, !1), b.addEventListener("touchmove", this.onTouchMove, !1), b.addEventListener("touchend", this.onTouchEnd, !1), b.addEventListener("touchcancel", this.onTouchCancel, !1), Event.prototype.stopImmediatePropagation || (b.removeEventListener = function (a, c, d) {
        var e = Node.prototype.removeEventListener;
        "click" === a ? e.call(b, a, c.hijacked || c, d) : e.call(b, a, c, d)
      }, b.addEventListener = function (a, c, d) {
        var e = Node.prototype.addEventListener;
        "click" === a ? e.call(b, a, c.hijacked || (c.hijacked = function (a) {
          a.propagationStopped || c(a)
        }), d) : e.call(b, a, c, d)
      }), "function" == typeof b.onclick && (e = b.onclick, b.addEventListener("click", function (a) {
        e(a)
      }, !1), b.onclick = null)
    }
  }

  var b = navigator.userAgent.indexOf("Windows Phone") >= 0, c = navigator.userAgent.indexOf("Android") > 0 && !b,
    d = /iP(ad|hone|od)/.test(navigator.userAgent) && !b, e = d && /OS 4_\d(_\d)?/.test(navigator.userAgent),
    f = d && /OS [6-7]_\d/.test(navigator.userAgent), g = navigator.userAgent.indexOf("BB10") > 0;
  a.prototype.needsClick = function (a) {
    switch (a.nodeName.toLowerCase()) {
      case"button":
      case"select":
      case"textarea":
        if (a.disabled) return !0;
        break;
      case"input":
        if (d && "file" === a.type || a.disabled) return !0;
        break;
      case"label":
      case"iframe":
      case"video":
        return !0
    }
    return /\bneedsclick\b/.test(a.className)
  }, a.prototype.needsFocus = function (a) {
    switch (a.nodeName.toLowerCase()) {
      case"textarea":
        return !0;
      case"select":
        return !c;
      case"input":
        switch (a.type) {
          case"button":
          case"checkbox":
          case"file":
          case"image":
          case"radio":
          case"submit":
            return !1
        }
        return !a.disabled && !a.readOnly;
      default:
        return /\bneedsfocus\b/.test(a.className)
    }
  }, a.prototype.sendClick = function (a, b) {
    var c, d;
    document.activeElement && document.activeElement !== a && document.activeElement.blur(), d = b.changedTouches[0], c = document.createEvent("MouseEvents"), c.initMouseEvent(this.determineEventType(a), !0, !0, window, 1, d.screenX, d.screenY, d.clientX, d.clientY, !1, !1, !1, !1, 0, null), c.forwardedTouchEvent = !0, a.dispatchEvent(c)
  }, a.prototype.determineEventType = function (a) {
    return c && "select" === a.tagName.toLowerCase() ? "mousedown" : "click"
  }, a.prototype.focus = function (a) {
    var b;
    d && a.setSelectionRange && 0 !== a.type.indexOf("date") && "time" !== a.type && "month" !== a.type ? (b = a.value.length, a.setSelectionRange(b, b)) : a.focus()
  }, a.prototype.updateScrollParent = function (a) {
    var b, c;
    if (b = a.fastClickScrollParent, !b || !b.contains(a)) {
      c = a;
      do {
        if (c.scrollHeight > c.offsetHeight) {
          b = c, a.fastClickScrollParent = c;
          break
        }
        c = c.parentElement
      } while (c)
    }
    b && (b.fastClickLastScrollTop = b.scrollTop)
  }, a.prototype.getTargetElementFromEventTarget = function (a) {
    return a.nodeType === Node.TEXT_NODE ? a.parentNode : a
  }, a.prototype.onTouchStart = function (a) {
    var b, c, f;
    if (a.targetTouches.length > 1) return !0;
    if (b = this.getTargetElementFromEventTarget(a.target), c = a.targetTouches[0], d) {
      if (f = window.getSelection(), f.rangeCount && !f.isCollapsed) return !0;
      if (!e) {
        if (c.identifier && c.identifier === this.lastTouchIdentifier) return a.preventDefault(), !1;
        this.lastTouchIdentifier = c.identifier, this.updateScrollParent(b)
      }
    }
    return this.trackingClick = !0, this.trackingClickStart = a.timeStamp, this.targetElement = b, this.touchStartX = c.pageX, this.touchStartY = c.pageY, a.timeStamp - this.lastClickTime < this.tapDelay && a.preventDefault(), !0
  }, a.prototype.touchHasMoved = function (a) {
    var b = a.changedTouches[0], c = this.touchBoundary;
    return Math.abs(b.pageX - this.touchStartX) > c || Math.abs(b.pageY - this.touchStartY) > c
  }, a.prototype.onTouchMove = function (a) {
    return !this.trackingClick || ((this.targetElement !== this.getTargetElementFromEventTarget(a.target) || this.touchHasMoved(a)) && (this.trackingClick = !1, this.targetElement = null), !0)
  }, a.prototype.findControl = function (a) {
    return void 0 !== a.control ? a.control : a.htmlFor ? document.getElementById(a.htmlFor) : a.querySelector("button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea")
  }, a.prototype.onTouchEnd = function (a) {
    var b, g, h, i, j, k = this.targetElement;
    if (!this.trackingClick) return !0;
    if (a.timeStamp - this.lastClickTime < this.tapDelay) return this.cancelNextClick = !0, !0;
    if (a.timeStamp - this.trackingClickStart > this.tapTimeout) return !0;
    if (this.cancelNextClick = !1, this.lastClickTime = a.timeStamp, g = this.trackingClickStart, this.trackingClick = !1, this.trackingClickStart = 0, f && (j = a.changedTouches[0], k = document.elementFromPoint(j.pageX - window.pageXOffset, j.pageY - window.pageYOffset) || k, k.fastClickScrollParent = this.targetElement.fastClickScrollParent), h = k.tagName.toLowerCase(), "label" === h) {
      if (b = this.findControl(k)) {
        if (this.focus(k), c) return !1;
        k = b
      }
    } else if (this.needsFocus(k)) return a.timeStamp - g > 100 || d && window.top !== window && "input" === h ? (this.targetElement = null, !1) : (this.focus(k), this.sendClick(k, a), d && "select" === h || (this.targetElement = null, a.preventDefault()), !1);
    return !(!d || e || (i = k.fastClickScrollParent, !i || i.fastClickLastScrollTop === i.scrollTop)) || (this.needsClick(k) || (a.preventDefault(), this.sendClick(k, a)), !1)
  }, a.prototype.onTouchCancel = function () {
    this.trackingClick = !1, this.targetElement = null
  }, a.prototype.onMouse = function (a) {
    return !this.targetElement || (!!a.forwardedTouchEvent || (!a.cancelable || (!(!this.needsClick(this.targetElement) || this.cancelNextClick) || (a.stopImmediatePropagation ? a.stopImmediatePropagation() : a.propagationStopped = !0, a.stopPropagation(), a.preventDefault(), !1))))
  }, a.prototype.onClick = function (a) {
    var b;
    return this.trackingClick ? (this.targetElement = null, this.trackingClick = !1, !0) : "submit" === a.target.type && 0 === a.detail || (b = this.onMouse(a), b || (this.targetElement = null), b)
  }, a.prototype.destroy = function () {
    var a = this.layer;
    c && (a.removeEventListener("mouseover", this.onMouse, !0), a.removeEventListener("mousedown", this.onMouse, !0), a.removeEventListener("mouseup", this.onMouse, !0)), a.removeEventListener("click", this.onClick, !0), a.removeEventListener("touchstart", this.onTouchStart, !1), a.removeEventListener("touchmove", this.onTouchMove, !1), a.removeEventListener("touchend", this.onTouchEnd, !1), a.removeEventListener("touchcancel", this.onTouchCancel, !1)
  }, a.notNeeded = function (a) {
    var b, d, e, f;
    if ("undefined" == typeof window.ontouchstart) return !0;
    if (d = +(/Chrome\/([0-9]+)/.exec(navigator.userAgent) || [, 0])[1]) {
      if (!c) return !0;
      if (b = document.querySelector("meta[name=viewport]")) {
        if (b.content.indexOf("user-scalable=no") !== -1) return !0;
        if (d > 31 && document.documentElement.scrollWidth <= window.outerWidth) return !0
      }
    }
    if (g && (e = navigator.userAgent.match(/Version\/([0-9]*)\.([0-9]*)/), e[1] >= 10 && e[2] >= 3 && (b = document.querySelector("meta[name=viewport]")))) {
      if (b.content.indexOf("user-scalable=no") !== -1) return !0;
      if (document.documentElement.scrollWidth <= window.outerWidth) return !0
    }
    return "none" === a.style.msTouchAction || "manipulation" === a.style.touchAction || (f = +(/Firefox\/([0-9]+)/.exec(navigator.userAgent) || [, 0])[1], !!(f >= 27 && (b = document.querySelector("meta[name=viewport]"), b && (b.content.indexOf("user-scalable=no") !== -1 || document.documentElement.scrollWidth <= window.outerWidth))) || ("none" === a.style.touchAction || "manipulation" === a.style.touchAction))
  }, a.attach = function (b, c) {
    return new a(b, c)
  }, "function" == typeof define && "object" == typeof define.amd && define.amd ? define(function () {
    return a
  }) : "undefined" != typeof module && module.exports ? (module.exports = a.attach, module.exports.FastClick = a) : window.FastClick = a
}();